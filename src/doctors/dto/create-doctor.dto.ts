export class CreateDoctorDto {
  doctor_name: string;

  doctor_surname: string;

  doctor_cardnumber: string;

  doctor_gender: string;

  doctor_position: string;

  doctor_phonenumber: string;

  doctor_address: string;
}
