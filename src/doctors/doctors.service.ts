import { Injectable } from '@nestjs/common';
import { CreateDoctorDto } from './dto/create-doctor.dto';
import { UpdateDoctorDto } from './dto/update-doctor.dto';
import { Doctor } from './entities/doctor.entity';
import { Repository } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';

@Injectable()
export class DoctorsService {
  constructor(
    @InjectRepository(Doctor)
    private doctorsRepository: Repository<Doctor>,
  ) {}

  create(createDoctorDto: CreateDoctorDto): Promise<Doctor> {
    const doctor: Doctor = new Doctor();
    doctor.doctor_name = createDoctorDto.doctor_name;
    doctor.doctor_surname = createDoctorDto.doctor_surname;
    doctor.doctor_cardnumber = createDoctorDto.doctor_cardnumber;
    doctor.doctor_gender = createDoctorDto.doctor_gender;
    doctor.doctor_position = createDoctorDto.doctor_position;
    doctor.doctor_phonenumber = createDoctorDto.doctor_phonenumber;
    doctor.doctor_address = createDoctorDto.doctor_address;
    return this.doctorsRepository.save(doctor);
  }

  findAll(): Promise<Doctor[]> {
    return this.doctorsRepository.find();
  }

  findOne(id: number): Promise<Doctor> {
    return this.doctorsRepository.findOneBy({ doctor_id: id });
  }

  async update(id: number, updateDoctorDto: UpdateDoctorDto) {
    const doctor = await this.doctorsRepository.findOneBy({ doctor_id: id });
    const updatedDoctor = { ...doctor, ...updateDoctorDto };
    return this.doctorsRepository.save(updatedDoctor);
  }

  async remove(id: number) {
    const doctor = await this.doctorsRepository.findOneBy({ doctor_id: id });
    return this.doctorsRepository.remove(doctor);
  }
}
