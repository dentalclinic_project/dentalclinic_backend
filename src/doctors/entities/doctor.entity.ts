import { Appointment } from 'src/appointments/entities/appointment.entity';
import { Dispensing } from 'src/dispensings/entities/dispensing.entity';
import { HisTreatment } from 'src/his_treatments/entities/his_treatment.entity';
import { Plan } from 'src/plans/entities/plan.entity';
import { Que } from 'src/que/entities/que.entity';
import { Treatment } from 'src/treatments/entities/treatment.entity';
import {
  Column,
  Entity,
  OneToMany,
  OneToOne,
  PrimaryGeneratedColumn,
} from 'typeorm';

@Entity()
export class Doctor {
  @PrimaryGeneratedColumn()
  doctor_id: number;

  @Column()
  doctor_name: string;

  @Column()
  doctor_surname: string;

  @Column()
  doctor_cardnumber: string;

  @Column()
  doctor_phonenumber: string;

  @Column()
  doctor_position: string;

  @Column()
  doctor_address: string;

  @Column()
  doctor_gender: string;

  @OneToMany(() => Appointment, (appoint) => appoint.doctor)
  appoint: Appointment;

  @OneToMany(() => Que, (que) => que.doctor)
  que: Que;

  @OneToMany(() => Treatment, (treatment) => treatment.doctor)
  treatment: Treatment;

  @OneToOne(() => Plan, (plan) => plan.doctor)
  plan: Plan;

  @OneToMany(() => HisTreatment, (hisTreatment) => hisTreatment.doctor)
  hisTreatment: HisTreatment;

  @OneToMany(() => Dispensing, (dispensing) => dispensing.doctor)
  dispensing: Dispensing;
}
