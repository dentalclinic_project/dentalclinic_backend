import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
} from '@nestjs/common';
import { TreatmentDetailsService } from './treatment-details.service';
import { CreateTreatmentDetailDto } from './dto/create-treatment-detail.dto';
import { UpdateTreatmentDetailDto } from './dto/update-treatment-detail.dto';

@Controller('treatment-details')
export class TreatmentDetailsController {
  constructor(
    private readonly treatmentDetailsService: TreatmentDetailsService,
  ) {}

  @Post()
  create(@Body() createTreatmentDetailDto: CreateTreatmentDetailDto) {
    return this.treatmentDetailsService.create(createTreatmentDetailDto);
  }

  @Get()
  findAll() {
    return this.treatmentDetailsService.findAll();
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.treatmentDetailsService.findOne(+id);
  }

  @Patch(':id')
  update(
    @Param('id') id: string,
    @Body() updateTreatmentDetailDto: UpdateTreatmentDetailDto,
  ) {
    return this.treatmentDetailsService.update(+id, updateTreatmentDetailDto);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.treatmentDetailsService.remove(+id);
  }
}
