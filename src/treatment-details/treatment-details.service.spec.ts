import { Test, TestingModule } from '@nestjs/testing';
import { TreatmentDetailsService } from './treatment-details.service';

describe('TreatmentDetailsService', () => {
  let service: TreatmentDetailsService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [TreatmentDetailsService],
    }).compile();

    service = module.get<TreatmentDetailsService>(TreatmentDetailsService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
