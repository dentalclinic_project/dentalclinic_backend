import { Module } from '@nestjs/common';
import { TreatmentDetailsService } from './treatment-details.service';
import { TreatmentDetailsController } from './treatment-details.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { TreatmentDetail } from './entities/treatment-detail.entity';
import { TreatmentType } from 'src/treatment-type/entities/treatment-type.entity';
import { HisTreatment } from 'src/his_treatments/entities/his_treatment.entity';
import { TypeDetail } from 'src/type-details/entities/type-detail.entity';

@Module({
  imports: [
    TypeOrmModule.forFeature([
      TreatmentDetail,
      TreatmentType,
      TypeDetail,
      HisTreatment,
    ]),
  ],
  controllers: [TreatmentDetailsController],
  providers: [TreatmentDetailsService],
})
export class TreatmentDetailsModule {}
