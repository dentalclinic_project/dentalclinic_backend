import { PartialType } from '@nestjs/mapped-types';
import { CreateTreatmentDetailDto } from './create-treatment-detail.dto';

export class UpdateTreatmentDetailDto extends PartialType(
  CreateTreatmentDetailDto,
) {}
