export class CreateTreatmentDetailDto {
  treatment_detail_type: string;

  treatment_detail_typedetail: string;

  treatment_detail_toothPosition: string;

  treatment_detail_price: number;

  treatment_detail_qty: number;

  // treatment_detail_hisTreatment: string;

  treatmenttypeId: number;

  typeDetailId: number;

  hisTreatmentId: number;
}
