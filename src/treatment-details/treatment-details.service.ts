import { Injectable, NotFoundException } from '@nestjs/common';
import { CreateTreatmentDetailDto } from './dto/create-treatment-detail.dto';
import { UpdateTreatmentDetailDto } from './dto/update-treatment-detail.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { TreatmentDetail } from './entities/treatment-detail.entity';
import { Repository } from 'typeorm';
import { TreatmentType } from 'src/treatment-type/entities/treatment-type.entity';
import { HisTreatment } from 'src/his_treatments/entities/his_treatment.entity';
import { TypeDetail } from 'src/type-details/entities/type-detail.entity';

@Injectable()
export class TreatmentDetailsService {
  constructor(
    @InjectRepository(TreatmentDetail)
    private treatmentDetailsRepository: Repository<TreatmentDetail>,
    @InjectRepository(TreatmentType)
    private treatmenttypeRepository: Repository<TreatmentType>,
    @InjectRepository(TypeDetail)
    private typeDetailRepository: Repository<TypeDetail>,
    @InjectRepository(HisTreatment)
    private hisTreatmentsRepository: Repository<HisTreatment>,
  ) {}

  async create(createTreatmentDetailDto: CreateTreatmentDetailDto) {
    const treatmenttype = await this.treatmenttypeRepository.findOneBy({
      treatmenttype_id: createTreatmentDetailDto.treatmenttypeId,
    });
    const typeDetail = await this.typeDetailRepository.findOneBy({
      typeDetail_id: createTreatmentDetailDto.typeDetailId,
    });
    const hisTreatment = await this.hisTreatmentsRepository.findOneBy({
      his_id: createTreatmentDetailDto.hisTreatmentId,
    });

    const treatmentDetail: TreatmentDetail = new TreatmentDetail();
    treatmentDetail.treatment_detail_type =
      createTreatmentDetailDto.treatment_detail_type;
    treatmentDetail.treatment_detail_typedetail =
      createTreatmentDetailDto.treatment_detail_typedetail;
    treatmentDetail.treatment_detail_toothPosition =
      createTreatmentDetailDto.treatment_detail_toothPosition;
    treatmentDetail.treatment_detail_qty =
      createTreatmentDetailDto.treatment_detail_qty;
    treatmentDetail.treatment_detail_price =
      createTreatmentDetailDto.treatment_detail_price;
    // treatmentDetail.treatment_detail_hisTreatment =
    //   createTreatmentDetailDto.treatment_detail_hisTreatment;
    treatmentDetail.treatmenttype = treatmenttype;
    treatmentDetail.typeDetail = typeDetail;
    treatmentDetail.hisTreatment = hisTreatment;

    await this.treatmentDetailsRepository.save(treatmentDetail);
    return await this.treatmentDetailsRepository.findOne({
      where: { treatment_detail_id: treatmentDetail.treatment_detail_id },
      relations: ['treatmenttype', 'hisTreatment', 'typeDetail'],
    });
  }

  findAll() {
    return this.treatmentDetailsRepository.find({
      relations: ['treatmenttype', 'hisTreatment', 'typeDetail'],
    });
  }

  findOne(id: number) {
    return this.treatmentDetailsRepository.findOneBy({
      treatment_detail_id: id,
    });
  }

  async update(
    treatment_detail_id: number,
    updateTreatmentDetailDto: UpdateTreatmentDetailDto,
  ) {
    const treatmentDetail = await this.treatmentDetailsRepository.findOneBy({
      treatment_detail_id,
    });
    if (!treatmentDetail) {
      throw new NotFoundException();
    }
    const updatedtreatmentDetail = {
      ...treatmentDetail,
      ...updateTreatmentDetailDto,
    };
    return this.treatmentDetailsRepository.save(updatedtreatmentDetail);
  }

  async remove(treatment_detail_id: number) {
    const treatmentDetail = await this.treatmentDetailsRepository.findOneBy({
      treatment_detail_id,
    });
    return this.treatmentDetailsRepository.remove(treatmentDetail);
  }
}
