import { Test, TestingModule } from '@nestjs/testing';
import { TreatmentDetailsController } from './treatment-details.controller';
import { TreatmentDetailsService } from './treatment-details.service';

describe('TreatmentDetailsController', () => {
  let controller: TreatmentDetailsController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [TreatmentDetailsController],
      providers: [TreatmentDetailsService],
    }).compile();

    controller = module.get<TreatmentDetailsController>(
      TreatmentDetailsController,
    );
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
