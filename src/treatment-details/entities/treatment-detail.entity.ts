import { HisTreatment } from 'src/his_treatments/entities/his_treatment.entity';
import { Plan } from 'src/plans/entities/plan.entity';
import { TreatmentType } from 'src/treatment-type/entities/treatment-type.entity';
import { TypeDetail } from 'src/type-details/entities/type-detail.entity';
import {
  Column,
  Entity,
  JoinColumn,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
} from 'typeorm';

@Entity()
export class TreatmentDetail {
  @PrimaryGeneratedColumn()
  treatment_detail_id: number;

  @Column()
  treatment_detail_type: string;

  @Column()
  treatment_detail_typedetail: string;

  @Column()
  treatment_detail_toothPosition: string;

  @Column()
  treatment_detail_qty: number;

  @Column()
  treatment_detail_price: number;

  // @Column()
  // treatment_detail_hisTreatment: string;

  @ManyToOne(
    () => TreatmentType,
    (treatmenttype) => treatmenttype.treatmentDetail,
  )
  @JoinColumn({ name: 'treatmenttypeId' })
  treatmenttype: TreatmentType;

  @ManyToOne(() => TypeDetail, (typeDetail) => typeDetail.treatmentDetail)
  @JoinColumn({ name: 'typeDetailId' })
  typeDetail: TypeDetail;

  @OneToMany(() => Plan, (plan) => plan.treatmentDetail)
  plan: Plan;

  @ManyToOne(() => HisTreatment, (hisTreatment) => hisTreatment.treatmentDetail)
  @JoinColumn({ name: 'hisTreatmentId' })
  hisTreatment: HisTreatment;
}
