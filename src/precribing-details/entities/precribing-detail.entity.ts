import { HisTreatment } from 'src/his_treatments/entities/his_treatment.entity';
import { Medicine } from 'src/medicines/entities/medicine.entity';
import {
  Column,
  Entity,
  PrimaryGeneratedColumn,
  ManyToOne,
  JoinColumn,
} from 'typeorm';

@Entity()
export class PrecribingDetail {
  @PrimaryGeneratedColumn()
  precribing_detail_id: number;

  @Column()
  precribing_detail_medicine: string;

  @Column()
  precribing_detail_qty: number;

  @Column()
  precribing_detail_price: number;

  // @Column()
  // precribing_detail_hisTreatment: string;

  @ManyToOne(() => Medicine, (medicine) => medicine.precribingDetail)
  @JoinColumn({ name: 'meditcineId' })
  medicine: Medicine;

  // @OneToMany(
  //   () => HisTreatment,
  //   (hisTreatment) => hisTreatment.precribingDetail,
  // )
  // hisTreatment: HisTreatment;

  @ManyToOne(
    () => HisTreatment,
    (hisTreatment) => hisTreatment.precribingDetail,
  )
  @JoinColumn({ name: 'hisTreatmentId' })
  hisTreatment: HisTreatment;
}
