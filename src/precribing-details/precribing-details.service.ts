import { Injectable, NotFoundException } from '@nestjs/common';
import { CreatePrecribingDetailDto } from './dto/create-precribing-detail.dto';
import { UpdatePrecribingDetailDto } from './dto/update-precribing-detail.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { PrecribingDetail } from './entities/precribing-detail.entity';
import { Repository } from 'typeorm';
import { Medicine } from 'src/medicines/entities/medicine.entity';
import { HisTreatment } from 'src/his_treatments/entities/his_treatment.entity';

@Injectable()
export class PrecribingDetailsService {
  constructor(
    @InjectRepository(PrecribingDetail)
    private precribingDetailsRepository: Repository<PrecribingDetail>,
    @InjectRepository(Medicine)
    private medicinesRepository: Repository<Medicine>,
    @InjectRepository(HisTreatment)
    private hisTreatmentsRepository: Repository<HisTreatment>,
  ) {}

  async create(createPrecribingDetailDto: CreatePrecribingDetailDto) {
    const medicine = await this.medicinesRepository.findOneBy({
      medicine_id: createPrecribingDetailDto.meditcineId,
    });
    const hisTreatment = await this.hisTreatmentsRepository.findOneBy({
      his_id: createPrecribingDetailDto.hisTreatmentId,
    });

    const precribingDetail: PrecribingDetail = new PrecribingDetail();
    precribingDetail.precribing_detail_medicine =
      createPrecribingDetailDto.precribing_detail_medicine;
    precribingDetail.precribing_detail_qty =
      createPrecribingDetailDto.precribing_detail_qty;
    precribingDetail.precribing_detail_price =
      createPrecribingDetailDto.precribing_detail_price;
    // precribingDetail.precribing_detail_hisTreatment =
    //   createPrecribingDetailDto.precribing_detail_hisTreatment;
    precribingDetail.medicine = medicine;
    precribingDetail.hisTreatment = hisTreatment;

    await this.precribingDetailsRepository.save(precribingDetail);
    return await this.precribingDetailsRepository.findOne({
      where: { precribing_detail_id: precribingDetail.precribing_detail_id },
      relations: ['medicine', 'hisTreatment'],
    });
  }

  findAll() {
    return this.precribingDetailsRepository.find({
      relations: ['medicine', 'hisTreatment'],
    });
  }

  findOne(id: number) {
    return this.precribingDetailsRepository.findOneBy({
      precribing_detail_id: id,
    });
  }

  async update(
    precribing_detail_id: number,
    updatePrecribingDetailDto: UpdatePrecribingDetailDto,
  ) {
    const precribingDetail = await this.precribingDetailsRepository.findOneBy({
      precribing_detail_id,
    });
    if (!precribingDetail) {
      throw new NotFoundException();
    }
    const updatedPrecribingDetail = {
      ...precribingDetail,
      ...updatePrecribingDetailDto,
    };
    return this.precribingDetailsRepository.save(updatedPrecribingDetail);
  }

  async remove(precribing_detail_id: number) {
    const precribingDetail = await this.precribingDetailsRepository.findOneBy({
      precribing_detail_id,
    });
    return this.precribingDetailsRepository.remove(precribingDetail);
  }
}
