import { Module } from '@nestjs/common';
import { PrecribingDetailsService } from './precribing-details.service';
import { PrecribingDetailsController } from './precribing-details.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { PrecribingDetail } from './entities/precribing-detail.entity';
import { Medicine } from 'src/medicines/entities/medicine.entity';
import { HisTreatment } from 'src/his_treatments/entities/his_treatment.entity';

@Module({
  imports: [
    TypeOrmModule.forFeature([PrecribingDetail, Medicine, HisTreatment]),
  ],
  controllers: [PrecribingDetailsController],
  providers: [PrecribingDetailsService],
})
export class PrecribingDetailsModule {}
