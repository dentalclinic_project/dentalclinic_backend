import { Test, TestingModule } from '@nestjs/testing';
import { PrecribingDetailsService } from './precribing-details.service';

describe('PrecribingDetailsService', () => {
  let service: PrecribingDetailsService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [PrecribingDetailsService],
    }).compile();

    service = module.get<PrecribingDetailsService>(PrecribingDetailsService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
