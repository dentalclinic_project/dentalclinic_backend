import { PartialType } from '@nestjs/mapped-types';
import { CreatePrecribingDetailDto } from './create-precribing-detail.dto';

export class UpdatePrecribingDetailDto extends PartialType(
  CreatePrecribingDetailDto,
) {}
