import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
} from '@nestjs/common';
import { PrecribingDetailsService } from './precribing-details.service';
import { CreatePrecribingDetailDto } from './dto/create-precribing-detail.dto';
import { UpdatePrecribingDetailDto } from './dto/update-precribing-detail.dto';

@Controller('precribing-details')
export class PrecribingDetailsController {
  constructor(
    private readonly precribingDetailsService: PrecribingDetailsService,
  ) {}

  @Post()
  create(@Body() createPrecribingDetailDto: CreatePrecribingDetailDto) {
    return this.precribingDetailsService.create(createPrecribingDetailDto);
  }

  @Get()
  findAll() {
    return this.precribingDetailsService.findAll();
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.precribingDetailsService.findOne(+id);
  }

  @Patch(':id')
  update(
    @Param('id') id: string,
    @Body() updatePrecribingDetailDto: UpdatePrecribingDetailDto,
  ) {
    return this.precribingDetailsService.update(+id, updatePrecribingDetailDto);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.precribingDetailsService.remove(+id);
  }
}
