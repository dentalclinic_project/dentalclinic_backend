import { Test, TestingModule } from '@nestjs/testing';
import { PrecribingDetailsController } from './precribing-details.controller';
import { PrecribingDetailsService } from './precribing-details.service';

describe('PrecribingDetailsController', () => {
  let controller: PrecribingDetailsController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [PrecribingDetailsController],
      providers: [PrecribingDetailsService],
    }).compile();

    controller = module.get<PrecribingDetailsController>(
      PrecribingDetailsController,
    );
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
