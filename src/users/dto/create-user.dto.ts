import { MinLength, Matches, IsNotEmpty } from 'class-validator';

export class CreateUserDto {
  @IsNotEmpty()
  @MinLength(3)
  user_login: string;

  @IsNotEmpty()
  @MinLength(5)
  user_name: string;

  @Matches(/^(?=.*\d)(?=.*[!@#$%^&*])(?=.*[a-z])(?=.*[A-Z]).{5,}$/)
  @MinLength(5)
  @IsNotEmpty()
  user_password: string;
}
