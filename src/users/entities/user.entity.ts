import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class User {
  @PrimaryGeneratedColumn()
  user_id: number;

  @Column()
  user_login: string;

  @Column()
  user_name: string;

  @Column()
  user_password: string;
}
