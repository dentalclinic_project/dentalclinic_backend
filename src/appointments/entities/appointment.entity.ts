import { Doctor } from 'src/doctors/entities/doctor.entity';
import { Patien } from 'src/patiens/entities/patien.entity';
import { TreatmentType } from 'src/treatment-type/entities/treatment-type.entity';
import {
  Column,
  Entity,
  JoinColumn,
  ManyToOne,
  OneToOne,
  PrimaryGeneratedColumn,
} from 'typeorm';

@Entity()
export class Appointment {
  @PrimaryGeneratedColumn()
  appoint_id: number;

  @Column()
  appoint_name: string;

  @Column()
  appoint_surname: string;

  @Column()
  appoint_cardnumber: string;

  @Column()
  appoint_phonenumber: string;

  @Column()
  appoint_date: string;

  @Column()
  appoint_time: string;

  @OneToOne(() => Patien, (patien) => patien.appointment)
  @JoinColumn({ name: 'patienId' })
  patien: Patien;

  @ManyToOne(() => TreatmentType, (treatmenttype) => treatmenttype.appointment)
  @JoinColumn({ name: 'treatmenttypeId' })
  treatmenttype: TreatmentType;

  @ManyToOne(() => Doctor, (doctor) => doctor.appoint)
  @JoinColumn({ name: 'doctorId' }) // ระบุคอลัมน์ที่ใช้ในการเชื่อมโยงระหว่าง Appointment และ Doctor
  doctor: Doctor;
}
