import { Module } from '@nestjs/common';
import { AppointmentsService } from './appointments.service';
import { AppointmentsController } from './appointments.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Appointment } from './entities/appointment.entity';
import { Doctor } from 'src/doctors/entities/doctor.entity';
import { Patien } from 'src/patiens/entities/patien.entity';
// import { Type } from 'src/types/entities/type.entity';
import { TreatmentType } from 'src/treatment-type/entities/treatment-type.entity';

@Module({
  imports: [
    TypeOrmModule.forFeature([Appointment, Doctor, Patien, TreatmentType]),
  ],
  controllers: [AppointmentsController],
  providers: [AppointmentsService],
})
export class AppointmentsModule {}
