export class CreateAppointmentDto {
  appoint_name: string;

  appoint_surname: string;

  appoint_cardnumber: string;

  appoint_phonenumber: string;

  appoint_date: string;

  appoint_time: string;

  patienId: number;

  treatmenttypeId: number;

  doctorId: number;
}
