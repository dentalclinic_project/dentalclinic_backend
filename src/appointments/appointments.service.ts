import { Injectable, NotFoundException } from '@nestjs/common';
import { CreateAppointmentDto } from './dto/create-appointment.dto';
import { UpdateAppointmentDto } from './dto/update-appointment.dto';
import { Appointment } from './entities/appointment.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Doctor } from 'src/doctors/entities/doctor.entity';
import { Patien } from 'src/patiens/entities/patien.entity';
import { TreatmentType } from 'src/treatment-type/entities/treatment-type.entity';

@Injectable()
export class AppointmentsService {
  constructor(
    @InjectRepository(Appointment)
    private appointsRepository: Repository<Appointment>,
    @InjectRepository(Doctor)
    private doctorsRepository: Repository<Doctor>,
    @InjectRepository(Patien)
    private patiensRepository: Repository<Patien>,
    @InjectRepository(TreatmentType)
    private treatmenttypeRepository: Repository<TreatmentType>,
  ) {}

  async create(createAppointmentDto: CreateAppointmentDto) {
    const doctor = await this.doctorsRepository.findOneBy({
      doctor_id: createAppointmentDto.doctorId,
    });
    const patien = await this.patiensRepository.findOneBy({
      patien_id: createAppointmentDto.patienId,
    });
    const treatmenttype = await this.treatmenttypeRepository.findOneBy({
      treatmenttype_id: createAppointmentDto.treatmenttypeId,
    });

    const appoint: Appointment = new Appointment();
    appoint.patien = patien;
    appoint.treatmenttype = treatmenttype;
    appoint.doctor = doctor;
    appoint.appoint_name = createAppointmentDto.appoint_name;
    appoint.appoint_surname = createAppointmentDto.appoint_surname;
    appoint.appoint_cardnumber = createAppointmentDto.appoint_cardnumber;
    appoint.appoint_phonenumber = createAppointmentDto.appoint_phonenumber;
    appoint.appoint_date = createAppointmentDto.appoint_date;
    appoint.appoint_time = createAppointmentDto.appoint_time;
    // appoint.patien.patienId = createAppointmentDto.patienId;
    // appoint.treatmenttype.treatmenttype_id =
    //   createAppointmentDto.treatmenttypeId;
    // appoint.doctor.doctor_id = createAppointmentDto.doctorId;
    await this.appointsRepository.save(appoint);
    return await this.appointsRepository.findOne({
      where: { appoint_id: appoint.appoint_id },
      relations: ['patien', 'treatmenttype', 'doctor'],
    });
  }

  // create(createAppointmentDto: CreateAppointmentDto) {
  //   const appoint = this.appointsRepository.save(createAppointmentDto);
  //   if (!appoint) {
  //     throw new NotFoundException();
  //   }
  //   return appoint;
  // }

  // findByDoctor(id: number) {
  //   return this.appointsRepository.find({ where: { doctorId: id } });
  // }

  findAll() {
    return this.appointsRepository.find({
      relations: ['patien', 'treatmenttype', 'doctor'],
    });
  }

  findOne(id: number) {
    return this.appointsRepository.findOneBy({ appoint_id: id });
  }

  async update(appoint_id: number, updateAppointmentDto: UpdateAppointmentDto) {
    const appointment = await this.appointsRepository.findOneBy({ appoint_id });
    if (!appointment) {
      throw new NotFoundException();
    }
    const updatedAppointment = { ...appointment, ...updateAppointmentDto };
    return this.appointsRepository.save(updatedAppointment);
  }

  async remove(appoint_id: number) {
    const appointment = await this.appointsRepository.findOneBy({ appoint_id });
    return this.appointsRepository.remove(appointment);
  }
}
