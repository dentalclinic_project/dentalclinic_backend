import { Injectable, NotFoundException } from '@nestjs/common';
import { CreateHisTreatmentDto } from './dto/create-his_treatment.dto';
import { UpdateHisTreatmentDto } from './dto/update-his_treatment.dto';
import { HisTreatment } from './entities/his_treatment.entity';
import { Repository } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';
import { Doctor } from 'src/doctors/entities/doctor.entity';
import { Patien } from 'src/patiens/entities/patien.entity';
import { TreatmentType } from 'src/treatment-type/entities/treatment-type.entity';
import { TreatmentDetail } from 'src/treatment-details/entities/treatment-detail.entity';
import { PrecribingDetail } from 'src/precribing-details/entities/precribing-detail.entity';

@Injectable()
export class HisTreatmentsService {
  constructor(
    @InjectRepository(HisTreatment)
    private hisTreatmentsRepository: Repository<HisTreatment>,
    @InjectRepository(Doctor)
    private doctorsRepository: Repository<Doctor>,
    @InjectRepository(Patien)
    private patiensRepository: Repository<Patien>,
    @InjectRepository(TreatmentType)
    private treatmenttypeRepository: Repository<TreatmentType>,
    @InjectRepository(TreatmentDetail)
    private treatmentDetailRepository: Repository<TreatmentDetail>,
    @InjectRepository(PrecribingDetail)
    private precribingDetailRepository: Repository<PrecribingDetail>,
  ) {}

  async create(createHisTreatmentDto: CreateHisTreatmentDto) {
    const doctor = await this.doctorsRepository.findOneBy({
      doctor_id: createHisTreatmentDto.doctorId,
    });
    const patien = await this.patiensRepository.findOneBy({
      patien_id: createHisTreatmentDto.patienId,
    });
    const treatmenttype = await this.treatmenttypeRepository.findOneBy({
      treatmenttype_id: createHisTreatmentDto.treatmenttypeId,
    });
    const treatmentDetail = await this.treatmentDetailRepository.findOneBy({
      treatment_detail_id: createHisTreatmentDto.treatmentDetailId,
    });
    const precribingDetail = await this.precribingDetailRepository.findOneBy({
      precribing_detail_id: createHisTreatmentDto.precribingDetailId,
    });

    const hisTreatment: HisTreatment = new HisTreatment();
    hisTreatment.his_patien = createHisTreatmentDto.his_patien;
    hisTreatment.his_date = new Date();
    // hisTreatment.his_time = createHisTreatmentDto.his_time;
    // hisTreatment.his_allergy = createHisTreatmentDto.his_allergy;
    // hisTreatment.his_treatmentType = createHisTreatmentDto.his_treatmentType;
    // hisTreatment.his_typeDetail = createHisTreatmentDto.his_typeDetail;
    // hisTreatment.his_drug = createHisTreatmentDto.his_drug;
    hisTreatment.his_doctor = createHisTreatmentDto.his_doctor;
    // hisTreatment.his_precribingDetail =
    //   createHisTreatmentDto.his_precribingDetail;
    // hisTreatment.his_teethPosition = createHisTreatmentDto.his_teethPosition;
    hisTreatment.doctor = doctor;
    hisTreatment.patien = patien;
    hisTreatment.treatmenttype = treatmenttype;
    hisTreatment.treatmentDetail = treatmentDetail;
    hisTreatment.precribingDetail = precribingDetail;

    await this.hisTreatmentsRepository.save(hisTreatment);
    return await this.hisTreatmentsRepository.findOne({
      where: { his_id: hisTreatment.his_id },
      relations: [
        'patien',
        'doctor',
        'treatmenttype',
        'treatmentDetail',
        'precribingDetail',
      ],
    });
  }

  findAll() {
    return this.hisTreatmentsRepository.find({
      relations: [
        'patien',
        'doctor',
        'treatmenttype',
        'treatmentDetail',
        'precribingDetail',
      ],
    });
  }

  async findOne(his_id: number) {
    const hisTreatment = await this.hisTreatmentsRepository.findOne({
      where: { his_id: his_id },
      relations: [
        'patien',
        'doctor',
        'treatmenttype',
        'treatmentDetail',
        'precribingDetail',
      ],
    });
    if (!hisTreatment) {
      throw new NotFoundException();
    }
    return hisTreatment;
  }

  async update(his_id: number, updateHisTreatmentDto: UpdateHisTreatmentDto) {
    const hisTreatment = await this.hisTreatmentsRepository.findOneBy({
      his_id,
    });
    if (!hisTreatment) {
      throw new NotFoundException();
    }
    const updatedHisTreatment = { ...hisTreatment, ...updateHisTreatmentDto };
    return this.hisTreatmentsRepository.save(updatedHisTreatment);
  }

  async remove(his_id: number) {
    const hisTreatment = await this.hisTreatmentsRepository.findOneBy({
      his_id,
    });
    return this.hisTreatmentsRepository.remove(hisTreatment);
  }
}
