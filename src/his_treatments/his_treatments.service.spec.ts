import { Test, TestingModule } from '@nestjs/testing';
import { HisTreatmentsService } from './his_treatments.service';

describe('HisTreatmentsService', () => {
  let service: HisTreatmentsService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [HisTreatmentsService],
    }).compile();

    service = module.get<HisTreatmentsService>(HisTreatmentsService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
