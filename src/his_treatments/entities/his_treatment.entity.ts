import { Doctor } from 'src/doctors/entities/doctor.entity';
import { Patien } from 'src/patiens/entities/patien.entity';
import { PrecribingDetail } from 'src/precribing-details/entities/precribing-detail.entity';
import { TreatmentDetail } from 'src/treatment-details/entities/treatment-detail.entity';
// import { PrecribingDetail } from 'src/precribing-details/entities/precribing-detail.entity';
import { TreatmentType } from 'src/treatment-type/entities/treatment-type.entity';

import {
  Column,
  Entity,
  JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn,
} from 'typeorm';

@Entity()
export class HisTreatment {
  @PrimaryGeneratedColumn()
  his_id: number;

  @Column()
  his_patien: string;

  @Column()
  his_date: Date;

  // @Column()
  // his_time: string;

  // @Column()
  // his_allergy: string;

  // @Column()
  // his_treatmentType: string;

  // @Column()
  // his_typeDetail: string;

  // @Column()
  // his_drug: string;

  @Column()
  his_doctor: string;

  // @Column()
  // his_precribingDetail: string;

  @ManyToOne(() => Doctor, (doctor) => doctor.hisTreatment)
  @JoinColumn({ name: 'doctorId' }) // ระบุคอลัมน์ที่ใช้ในการเชื่อมโยงระหว่าง Appointment และ Doctor
  doctor: Doctor;

  @ManyToOne(() => Patien, (patien) => patien.hisTreatment)
  @JoinColumn({ name: 'patienId' })
  patien: Patien;

  @ManyToOne(() => TreatmentType, (treatmenttype) => treatmenttype.hisTreatment)
  @JoinColumn({ name: 'treatmenttypeId' })
  treatmenttype: TreatmentType;

  @ManyToOne(
    () => TreatmentDetail,
    (treatmentDetail) => treatmentDetail.hisTreatment,
  )
  @JoinColumn({ name: 'treatmentDetailId' })
  treatmentDetail: TreatmentDetail;

  @ManyToOne(
    () => PrecribingDetail,
    (precribingDetail) => precribingDetail.hisTreatment,
  )
  @JoinColumn({ name: 'precribingDetailId' })
  precribingDetail: PrecribingDetail;

  // @OneToMany(
  //   () => PrecribingDetail,
  //   (precribingDetail) => precribingDetail.hisTreatment,
  // )
  // precribingDetail: PrecribingDetail;
}
