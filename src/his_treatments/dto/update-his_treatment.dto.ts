import { PartialType } from '@nestjs/mapped-types';
import { CreateHisTreatmentDto } from './create-his_treatment.dto';

export class UpdateHisTreatmentDto extends PartialType(CreateHisTreatmentDto) {}
