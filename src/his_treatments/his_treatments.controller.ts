import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
} from '@nestjs/common';
import { HisTreatmentsService } from './his_treatments.service';
import { CreateHisTreatmentDto } from './dto/create-his_treatment.dto';
import { UpdateHisTreatmentDto } from './dto/update-his_treatment.dto';

@Controller('his-treatments')
export class HisTreatmentsController {
  constructor(private readonly hisTreatmentsService: HisTreatmentsService) {}

  @Post()
  create(@Body() createHisTreatmentDto: CreateHisTreatmentDto) {
    return this.hisTreatmentsService.create(createHisTreatmentDto);
  }

  @Get()
  findAll() {
    return this.hisTreatmentsService.findAll();
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.hisTreatmentsService.findOne(+id);
  }

  @Patch(':id')
  update(
    @Param('id') id: string,
    @Body() updateHisTreatmentDto: UpdateHisTreatmentDto,
  ) {
    return this.hisTreatmentsService.update(+id, updateHisTreatmentDto);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.hisTreatmentsService.remove(+id);
  }
}
