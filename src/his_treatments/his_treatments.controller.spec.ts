import { Test, TestingModule } from '@nestjs/testing';
import { HisTreatmentsController } from './his_treatments.controller';
import { HisTreatmentsService } from './his_treatments.service';

describe('HisTreatmentsController', () => {
  let controller: HisTreatmentsController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [HisTreatmentsController],
      providers: [HisTreatmentsService],
    }).compile();

    controller = module.get<HisTreatmentsController>(HisTreatmentsController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
