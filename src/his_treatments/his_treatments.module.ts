import { Module } from '@nestjs/common';
import { HisTreatmentsService } from './his_treatments.service';
import { HisTreatmentsController } from './his_treatments.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { HisTreatment } from './entities/his_treatment.entity';
import { Doctor } from 'src/doctors/entities/doctor.entity';
import { Patien } from 'src/patiens/entities/patien.entity';
import { TreatmentType } from 'src/treatment-type/entities/treatment-type.entity';
import { PrecribingDetail } from 'src/precribing-details/entities/precribing-detail.entity';
import { TreatmentDetail } from 'src/treatment-details/entities/treatment-detail.entity';

@Module({
  imports: [
    TypeOrmModule.forFeature([
      HisTreatment,
      Doctor,
      Patien,
      TreatmentType,
      TreatmentDetail,
      PrecribingDetail,
    ]),
  ],
  controllers: [HisTreatmentsController],
  providers: [HisTreatmentsService],
})
export class HisTreatmentsModule {}
