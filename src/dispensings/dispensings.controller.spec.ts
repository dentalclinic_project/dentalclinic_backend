import { Test, TestingModule } from '@nestjs/testing';
import { DispensingsController } from './dispensings.controller';
import { DispensingsService } from './dispensings.service';

describe('DispensingsController', () => {
  let controller: DispensingsController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [DispensingsController],
      providers: [DispensingsService],
    }).compile();

    controller = module.get<DispensingsController>(DispensingsController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
