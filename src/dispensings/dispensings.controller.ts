import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
} from '@nestjs/common';
import { DispensingsService } from './dispensings.service';
import { CreateDispensingDto } from './dto/create-dispensing.dto';
import { UpdateDispensingDto } from './dto/update-dispensing.dto';

@Controller('dispensings')
export class DispensingsController {
  constructor(private readonly dispensingsService: DispensingsService) {}

  @Post()
  create(@Body() createDispensingDto: CreateDispensingDto) {
    return this.dispensingsService.create(createDispensingDto);
  }

  @Get()
  findAll() {
    return this.dispensingsService.findAll();
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.dispensingsService.findOne(+id);
  }

  @Patch(':id')
  update(
    @Param('id') id: string,
    @Body() updateDispensingDto: UpdateDispensingDto,
  ) {
    return this.dispensingsService.update(+id, updateDispensingDto);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.dispensingsService.remove(+id);
  }
}
