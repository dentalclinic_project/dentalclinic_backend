import { Test, TestingModule } from '@nestjs/testing';
import { DispensingsService } from './dispensings.service';

describe('DispensingsService', () => {
  let service: DispensingsService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [DispensingsService],
    }).compile();

    service = module.get<DispensingsService>(DispensingsService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
