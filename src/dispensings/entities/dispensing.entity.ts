import { Counter } from 'src/counters/entities/counter.entity';
import { Doctor } from 'src/doctors/entities/doctor.entity';
import { Medicine } from 'src/medicines/entities/medicine.entity';
import { Patien } from 'src/patiens/entities/patien.entity';
import { Payment } from 'src/payments/entities/payment.entity';
import {
  Column,
  Entity,
  JoinColumn,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
} from 'typeorm';

@Entity()
export class Dispensing {
  @PrimaryGeneratedColumn()
  dispensing_id: number;

  @Column()
  dispensing_patien: string;

  @Column()
  dispensing_date: string;

  @Column()
  dispensing_medicine: string;

  @Column()
  dispensing_qty: number;

  @Column()
  dispensing_price: number;

  @Column()
  dispensing_counter: string;

  @Column()
  dispensing_doctor: string;

  @ManyToOne(() => Patien, (patien) => patien.dispensing)
  @JoinColumn({ name: 'patienId' })
  patien: Patien;

  @ManyToOne(() => Medicine, (medicine) => medicine.dispensing)
  @JoinColumn({ name: 'medicineId' })
  medicine: Medicine;

  @ManyToOne(() => Counter, (counter) => counter.dispensing)
  @JoinColumn({ name: 'counterId' })
  counter: Counter;

  @ManyToOne(() => Doctor, (doctor) => doctor.dispensing)
  @JoinColumn({ name: 'doctorId' })
  doctor: Doctor;

  @OneToMany(() => Payment, (payment) => payment.dispensing)
  payment: Payment;
}
