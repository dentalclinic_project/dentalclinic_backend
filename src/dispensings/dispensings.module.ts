import { Module } from '@nestjs/common';
import { DispensingsService } from './dispensings.service';
import { DispensingsController } from './dispensings.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Dispensing } from './entities/dispensing.entity';
import { Medicine } from 'src/medicines/entities/medicine.entity';
import { Doctor } from 'src/doctors/entities/doctor.entity';
import { Counter } from 'src/counters/entities/counter.entity';
import { Patien } from 'src/patiens/entities/patien.entity';

@Module({
  imports: [
    TypeOrmModule.forFeature([Dispensing, Patien, Medicine, Doctor, Counter]),
  ],
  controllers: [DispensingsController],
  providers: [DispensingsService],
})
export class DispensingsModule {}
