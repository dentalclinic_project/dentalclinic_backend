import { Injectable, NotFoundException } from '@nestjs/common';
import { CreateDispensingDto } from './dto/create-dispensing.dto';
import { UpdateDispensingDto } from './dto/update-dispensing.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { Dispensing } from './entities/dispensing.entity';
import { Repository } from 'typeorm';
import { Medicine } from 'src/medicines/entities/medicine.entity';
import { Doctor } from 'src/doctors/entities/doctor.entity';
import { Counter } from 'src/counters/entities/counter.entity';
import { Patien } from 'src/patiens/entities/patien.entity';

@Injectable()
export class DispensingsService {
  constructor(
    @InjectRepository(Dispensing)
    private dispensingsRepository: Repository<Dispensing>,
    @InjectRepository(Patien)
    private patiensRepository: Repository<Patien>,
    @InjectRepository(Medicine)
    private medicinesRepository: Repository<Medicine>,
    @InjectRepository(Doctor)
    private doctorsRepository: Repository<Doctor>,
    @InjectRepository(Counter)
    private countersRepository: Repository<Counter>,
  ) {}

  async create(createDispensingDto: CreateDispensingDto) {
    const patien = await this.patiensRepository.findOneBy({
      patien_id: createDispensingDto.patienId,
    });
    const medicine = await this.medicinesRepository.findOneBy({
      medicine_id: createDispensingDto.medicineId,
    });
    const doctor = await this.doctorsRepository.findOneBy({
      doctor_id: createDispensingDto.doctorId,
    });
    const counter = await this.countersRepository.findOneBy({
      counter_id: createDispensingDto.counterId,
    });

    const dispensing: Dispensing = new Dispensing();
    dispensing.dispensing_patien = createDispensingDto.dispensing_patien;
    dispensing.dispensing_date = createDispensingDto.dispensing_date;
    dispensing.dispensing_medicine = createDispensingDto.dispensing_medicine;
    dispensing.dispensing_qty = createDispensingDto.dispensing_qty;
    dispensing.dispensing_price = createDispensingDto.dispensing_price;
    dispensing.dispensing_counter = createDispensingDto.dispensing_counter;
    dispensing.dispensing_doctor = createDispensingDto.dispensing_doctor;
    dispensing.patien = patien;
    dispensing.medicine = medicine;
    dispensing.counter = counter;
    dispensing.doctor = doctor;

    await this.dispensingsRepository.save(dispensing);
    return await this.dispensingsRepository.findOne({
      where: { dispensing_id: dispensing.dispensing_id },
      relations: ['patien', 'doctor', 'medicine', 'counter'],
    });
  }

  findAll() {
    return this.dispensingsRepository.find({
      relations: ['patien', 'doctor', 'medicine', 'counter'],
    });
  }

  findOne(id: number) {
    return this.dispensingsRepository.findOneBy({ dispensing_id: id });
  }

  async update(
    dispensing_id: number,
    updateDispensingDto: UpdateDispensingDto,
  ) {
    const dispensing = await this.dispensingsRepository.findOneBy({
      dispensing_id,
    });
    if (!dispensing) {
      throw new NotFoundException();
    }
    const updatedDispensing = { ...dispensing, ...updateDispensingDto };
    return this.dispensingsRepository.save(updatedDispensing);
  }

  async remove(dispensing_id: number) {
    const dispensing = await this.dispensingsRepository.findOneBy({
      dispensing_id,
    });
    return this.dispensingsRepository.remove(dispensing);
  }
}
