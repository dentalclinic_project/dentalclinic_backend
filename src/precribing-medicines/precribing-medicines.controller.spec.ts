import { Test, TestingModule } from '@nestjs/testing';
import { PrecribingMedicinesController } from './precribing-medicines.controller';
import { PrecribingMedicinesService } from './precribing-medicines.service';

describe('PrecribingMedicinesController', () => {
  let controller: PrecribingMedicinesController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [PrecribingMedicinesController],
      providers: [PrecribingMedicinesService],
    }).compile();

    controller = module.get<PrecribingMedicinesController>(
      PrecribingMedicinesController,
    );
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
