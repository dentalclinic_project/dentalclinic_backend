import { Module } from '@nestjs/common';
import { PrecribingMedicinesService } from './precribing-medicines.service';
import { PrecribingMedicinesController } from './precribing-medicines.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { PrecribingMedicine } from './entities/precribing-medicine.entity';
import { Counter } from 'src/counters/entities/counter.entity';
import { Patien } from 'src/patiens/entities/patien.entity';

@Module({
  imports: [TypeOrmModule.forFeature([PrecribingMedicine, Patien, Counter])],
  controllers: [PrecribingMedicinesController],
  providers: [PrecribingMedicinesService],
})
export class PrecribingMedicinesModule {}
