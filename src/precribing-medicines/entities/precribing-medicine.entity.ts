import { Counter } from 'src/counters/entities/counter.entity';
import { Patien } from 'src/patiens/entities/patien.entity';
import { Precription } from 'src/precriptions/entities/precription.entity';
import {
  Column,
  Entity,
  PrimaryGeneratedColumn,
  ManyToOne,
  JoinColumn,
  OneToMany,
} from 'typeorm';

@Entity()
export class PrecribingMedicine {
  @PrimaryGeneratedColumn()
  pre_medicine_id: number;

  @Column()
  pre_medicine_date: string;

  @Column()
  pre_medicine_patien: string;

  @Column()
  pre_medicine_counter: string;

  @Column()
  pre_medicine_total: number;

  @ManyToOne(() => Patien, (patien) => patien.preMedicine)
  @JoinColumn({ name: 'patienId' })
  patien: Patien;

  @ManyToOne(() => Counter, (counter) => counter.preMedicine)
  @JoinColumn({ name: 'counterId' })
  counter: Counter;

  @OneToMany(() => Precription, (precription) => precription.preMedicine)
  precription: Precription;
}
