import { Test, TestingModule } from '@nestjs/testing';
import { PrecribingMedicinesService } from './precribing-medicines.service';

describe('PrecribingMedicinesService', () => {
  let service: PrecribingMedicinesService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [PrecribingMedicinesService],
    }).compile();

    service = module.get<PrecribingMedicinesService>(
      PrecribingMedicinesService,
    );
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
