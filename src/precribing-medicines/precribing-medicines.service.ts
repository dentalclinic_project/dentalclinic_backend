import { Injectable, NotFoundException } from '@nestjs/common';
import { CreatePrecribingMedicineDto } from './dto/create-precribing-medicine.dto';
import { UpdatePrecribingMedicineDto } from './dto/update-precribing-medicine.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { PrecribingMedicine } from './entities/precribing-medicine.entity';
import { Repository } from 'typeorm';
import { Patien } from 'src/patiens/entities/patien.entity';
import { Counter } from 'src/counters/entities/counter.entity';

@Injectable()
export class PrecribingMedicinesService {
  constructor(
    @InjectRepository(PrecribingMedicine)
    private preMedicinesRepository: Repository<PrecribingMedicine>,
    @InjectRepository(Patien)
    private patiensRepository: Repository<Patien>,
    @InjectRepository(Counter)
    private countersRepository: Repository<Counter>,
  ) {}

  async create(createPrecribingMedicineDto: CreatePrecribingMedicineDto) {
    const patien = await this.patiensRepository.findOneBy({
      patien_id: createPrecribingMedicineDto.patienId,
    });
    const counter = await this.countersRepository.findOneBy({
      counter_id: createPrecribingMedicineDto.patienId,
    });

    const preMedicine: PrecribingMedicine = new PrecribingMedicine();
    preMedicine.pre_medicine_date =
      createPrecribingMedicineDto.pre_medicine_date;
    preMedicine.pre_medicine_patien =
      createPrecribingMedicineDto.pre_medicine_patien;
    preMedicine.pre_medicine_counter =
      createPrecribingMedicineDto.pre_medicine_counter;
    preMedicine.pre_medicine_total =
      createPrecribingMedicineDto.pre_medicine_total;
    preMedicine.patien = patien;
    preMedicine.counter = counter;

    await this.preMedicinesRepository.save(preMedicine);
    return await this.preMedicinesRepository.findOne({
      where: { pre_medicine_id: preMedicine.pre_medicine_id },
      relations: ['patien', 'counter'],
    });
  }

  findAll() {
    return this.preMedicinesRepository.find({
      relations: ['patien', 'counter'],
    });
  }

  findOne(id: number) {
    return this.preMedicinesRepository.findOneBy({ pre_medicine_id: id });
  }

  async update(
    pre_medicine_id: number,
    updatePrecribingMedicineDto: UpdatePrecribingMedicineDto,
  ) {
    const preMedicine = await this.preMedicinesRepository.findOneBy({
      pre_medicine_id,
    });
    if (!preMedicine) {
      throw new NotFoundException();
    }
    const updatedPreMedicine = {
      ...preMedicine,
      ...updatePrecribingMedicineDto,
    };
    return this.preMedicinesRepository.save(updatedPreMedicine);
  }

  async remove(pre_medicine_id: number) {
    const preMedicine = await this.preMedicinesRepository.findOneBy({
      pre_medicine_id,
    });
    return this.preMedicinesRepository.remove(preMedicine);
  }
}
