import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
} from '@nestjs/common';
import { PrecribingMedicinesService } from './precribing-medicines.service';
import { CreatePrecribingMedicineDto } from './dto/create-precribing-medicine.dto';
import { UpdatePrecribingMedicineDto } from './dto/update-precribing-medicine.dto';

@Controller('precribing-medicines')
export class PrecribingMedicinesController {
  constructor(
    private readonly precribingMedicinesService: PrecribingMedicinesService,
  ) {}

  @Post()
  create(@Body() createPrecribingMedicineDto: CreatePrecribingMedicineDto) {
    return this.precribingMedicinesService.create(createPrecribingMedicineDto);
  }

  @Get()
  findAll() {
    return this.precribingMedicinesService.findAll();
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.precribingMedicinesService.findOne(+id);
  }

  @Patch(':id')
  update(
    @Param('id') id: string,
    @Body() updatePrecribingMedicineDto: UpdatePrecribingMedicineDto,
  ) {
    return this.precribingMedicinesService.update(
      +id,
      updatePrecribingMedicineDto,
    );
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.precribingMedicinesService.remove(+id);
  }
}
