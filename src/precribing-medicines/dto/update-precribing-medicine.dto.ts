import { PartialType } from '@nestjs/mapped-types';
import { CreatePrecribingMedicineDto } from './create-precribing-medicine.dto';

export class UpdatePrecribingMedicineDto extends PartialType(
  CreatePrecribingMedicineDto,
) {}
