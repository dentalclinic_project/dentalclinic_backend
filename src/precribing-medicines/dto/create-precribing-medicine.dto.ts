export class CreatePrecribingMedicineDto {
  pre_medicine_date: string;

  pre_medicine_patien: string;

  pre_medicine_counter: string;

  pre_medicine_total: number;

  patienId: number;

  counterId: number;
}
