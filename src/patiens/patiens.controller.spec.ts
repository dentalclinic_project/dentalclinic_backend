import { Test, TestingModule } from '@nestjs/testing';
import { PatiensController } from './patiens.controller';
import { PatiensService } from './patiens.service';

describe('PatiensController', () => {
  let controller: PatiensController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [PatiensController],
      providers: [PatiensService],
    }).compile();

    controller = module.get<PatiensController>(PatiensController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
