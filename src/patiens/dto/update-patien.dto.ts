import { PartialType } from '@nestjs/mapped-types';
import { CreatePatienDto } from './create-patien.dto';

export class UpdatePatienDto extends PartialType(CreatePatienDto) {}
