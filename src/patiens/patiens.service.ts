import { Injectable } from '@nestjs/common';
import { CreatePatienDto } from './dto/create-patien.dto';
import { UpdatePatienDto } from './dto/update-patien.dto';
import { Patien } from './entities/patien.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

@Injectable()
export class PatiensService {
  constructor(
    @InjectRepository(Patien)
    private patiensRepository: Repository<Patien>,
  ) {}

  create(createPatienDto: CreatePatienDto): Promise<Patien> {
    const patien: Patien = new Patien();
    patien.patien_name = createPatienDto.patien_name;
    patien.patien_surname = createPatienDto.patien_surname;
    patien.patien_cardnumber = createPatienDto.patien_cardnumber;
    patien.patien_birth = createPatienDto.patien_birth;
    patien.patien_phonenumber = createPatienDto.patien_phonenumber;
    patien.patien_age = createPatienDto.patien_age;
    patien.patien_gender = createPatienDto.patien_gender;
    patien.patien_address = createPatienDto.patien_address;
    patien.patien_drugAllergy = createPatienDto.patien_drugAllergy;
    patien.patien_congenitalDisease = createPatienDto.patien_congenitalDisease;
    return this.patiensRepository.save(patien);
  }

  findAll(): Promise<Patien[]> {
    return this.patiensRepository.find();
  }
  // findAll(query) {
  //   const search = query.search;
  //   return this.patiensRepository.find({
  //     where: [
  //       { patien: search },
  //       { patien: { patien_name: Like(%${search}%) } },
  //       { patien: { patien_surname: Like(%${search}%) } },
  //     ],

  //   });
  // }

  findOne(id: number): Promise<Patien> {
    return this.patiensRepository.findOneBy({ patien_id: id });
  }

  async update(id: number, updatePatienDto: UpdatePatienDto) {
    const patien = await this.patiensRepository.findOneBy({ patien_id: id });
    const updatedPatien = { ...patien, ...updatePatienDto };
    return this.patiensRepository.save(updatedPatien);
  }

  async remove(id: number) {
    const patien = await this.patiensRepository.findOneBy({ patien_id: id });
    return this.patiensRepository.remove(patien);
  }
}
