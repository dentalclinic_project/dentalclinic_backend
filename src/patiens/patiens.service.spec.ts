import { Test, TestingModule } from '@nestjs/testing';
import { PatiensService } from './patiens.service';

describe('PatiensService', () => {
  let service: PatiensService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [PatiensService],
    }).compile();

    service = module.get<PatiensService>(PatiensService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
