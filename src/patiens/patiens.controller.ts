import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
} from '@nestjs/common';
import { PatiensService } from './patiens.service';
import { CreatePatienDto } from './dto/create-patien.dto';
import { UpdatePatienDto } from './dto/update-patien.dto';

@Controller('patiens')
export class PatiensController {
  constructor(private readonly patiensService: PatiensService) {}

  @Post()
  create(@Body() createPatienDto: CreatePatienDto) {
    return this.patiensService.create(createPatienDto);
  }

  @Get()
  findAll() {
    return this.patiensService.findAll();
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.patiensService.findOne(+id);
  }

  @Patch(':id')
  update(@Param('id') id: string, @Body() updatePatienDto: UpdatePatienDto) {
    return this.patiensService.update(+id, updatePatienDto);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.patiensService.remove(+id);
  }
}
