import { Module } from '@nestjs/common';
import { PatiensService } from './patiens.service';
import { PatiensController } from './patiens.controller';
import { Patien } from './entities/patien.entity';
import { TypeOrmModule } from '@nestjs/typeorm';

@Module({
  imports: [TypeOrmModule.forFeature([Patien])],
  controllers: [PatiensController],
  providers: [PatiensService],
})
export class PatiensModule {}
