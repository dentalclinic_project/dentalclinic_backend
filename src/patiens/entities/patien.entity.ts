import { Appointment } from 'src/appointments/entities/appointment.entity';
import { Dispensing } from 'src/dispensings/entities/dispensing.entity';
import { HisTreatment } from 'src/his_treatments/entities/his_treatment.entity';
import { Payment } from 'src/payments/entities/payment.entity';
import { Plan } from 'src/plans/entities/plan.entity';
import { PrecribingMedicine } from 'src/precribing-medicines/entities/precribing-medicine.entity';
import { Que } from 'src/que/entities/que.entity';
import { Treatment } from 'src/treatments/entities/treatment.entity';
import { Column, Entity, OneToMany, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class Patien {
  @PrimaryGeneratedColumn()
  patien_id: number;

  @Column()
  patien_name: string;

  @Column()
  patien_surname: string;

  @Column()
  patien_cardnumber: string;

  @Column()
  patien_birth: string;

  @Column()
  patien_phonenumber: string;

  @Column()
  patien_age: number;

  @Column()
  patien_gender: string;

  @Column()
  patien_address: string;

  @Column()
  patien_drugAllergy: string;

  @Column()
  patien_congenitalDisease: string;

  @OneToMany(() => Appointment, (appointment) => appointment.patien)
  appointment: Appointment;

  @OneToMany(() => Que, (que) => que.patien)
  que: Que;

  @OneToMany(() => Treatment, (treatment) => treatment.patien)
  treatment: Treatment;

  @OneToMany(() => Plan, (plan) => plan.patien)
  plan: Plan;

  @OneToMany(() => HisTreatment, (hisTreatment) => hisTreatment.patien)
  hisTreatment: HisTreatment;

  @OneToMany(() => PrecribingMedicine, (preMedicine) => preMedicine.patien)
  preMedicine: PrecribingMedicine;

  @OneToMany(() => Dispensing, (dispensing) => dispensing.patien)
  dispensing: Dispensing;

  @OneToMany(() => Payment, (payment) => payment.patien)
  payment: Payment;
}
