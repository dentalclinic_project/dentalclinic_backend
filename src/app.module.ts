import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { UsersModule } from './users/users.module';
import { TypeOrmModule } from '@nestjs/typeorm';
import { User } from './users/entities/user.entity';
import { DataSource } from 'typeorm';
import { PatiensModule } from './patiens/patiens.module';
import { Patien } from './patiens/entities/patien.entity';
import { DoctorsModule } from './doctors/doctors.module';
import { Doctor } from './doctors/entities/doctor.entity';
import { CountersModule } from './counters/counters.module';
import { Counter } from './counters/entities/counter.entity';
import { AppointmentsModule } from './appointments/appointments.module';
import { Appointment } from './appointments/entities/appointment.entity';
import { TreatmentTypeModule } from './treatment-type/treatment-type.module';
import { TreatmentType } from './treatment-type/entities/treatment-type.entity';
import { QueModule } from './que/que.module';
import { Que } from './que/entities/que.entity';
import { TreatmentsModule } from './treatments/treatments.module';
import { Treatment } from './treatments/entities/treatment.entity';
import { PlansModule } from './plans/plans.module';
import { Plan } from './plans/entities/plan.entity';
import { HisTreatmentsModule } from './his_treatments/his_treatments.module';
import { TreatmentDetail } from './treatment-details/entities/treatment-detail.entity';
import { TreatmentDetailsModule } from './treatment-details/treatment-details.module';
import { HisTreatment } from './his_treatments/entities/his_treatment.entity';
import { PrecriptionsModule } from './precriptions/precriptions.module';
import { PrecribingMedicinesModule } from './precribing-medicines/precribing-medicines.module';
import { MedicinesModule } from './medicines/medicines.module';
import { PrecribingDetailsModule } from './precribing-details/precribing-details.module';
import { RecieptsModule } from './reciepts/reciepts.module';
import { PaymentsModule } from './payments/payments.module';
import { PrecribingMedicine } from './precribing-medicines/entities/precribing-medicine.entity';
import { Precription } from './precriptions/entities/precription.entity';
import { MedicineTypesModule } from './medicine-types/medicine-types.module';
import { MedicineType } from './medicine-types/entities/medicine-type.entity';
import { Medicine } from './medicines/entities/medicine.entity';
import { PrecribingDetail } from './precribing-details/entities/precribing-detail.entity';
import { DispensingsModule } from './dispensings/dispensings.module';
import { Dispensing } from './dispensings/entities/dispensing.entity';
import { Payment } from './payments/entities/payment.entity';
import { Reciept } from './reciepts/entities/reciept.entity';
import { TypeDetailsModule } from './type-details/type-details.module';
import { TypeDetail } from './type-details/entities/type-detail.entity';

@Module({
  imports: [
    UsersModule,
    TypeOrmModule.forRoot({
      type: 'sqlite',
      database: 'db.sqlite',
      synchronize: true,
      logging: false,
      entities: [
        User,
        Patien,
        Doctor,
        Counter,
        Appointment,
        TreatmentType,
        Que,
        Treatment,
        Plan,
        TreatmentDetail,
        HisTreatment,
        PrecribingMedicine,
        Precription,
        MedicineType,
        Medicine,
        PrecribingDetail,
        Dispensing,
        Payment,
        Reciept,
        TypeDetail,
      ],
      subscribers: [],
      migrations: [],
    }),
    PatiensModule,
    DoctorsModule,
    CountersModule,
    AppointmentsModule,
    TreatmentTypeModule,
    QueModule,
    TreatmentsModule,
    PlansModule,
    HisTreatmentsModule,
    TreatmentDetailsModule,
    HisTreatmentsModule,
    PrecriptionsModule,
    PrecribingMedicinesModule,
    MedicinesModule,
    PrecribingDetailsModule,
    RecieptsModule,
    PaymentsModule,
    MedicineTypesModule,
    MedicineType,
    Medicine,
    DispensingsModule,
    TypeDetailsModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {
  constructor(private dataSource: DataSource) {}
}
