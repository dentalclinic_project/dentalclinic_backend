import { Module } from '@nestjs/common';
import { PrecriptionsService } from './precriptions.service';
import { PrecriptionsController } from './precriptions.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Precription } from './entities/precription.entity';
import { PrecribingMedicine } from 'src/precribing-medicines/entities/precribing-medicine.entity';

@Module({
  imports: [TypeOrmModule.forFeature([Precription, PrecribingMedicine])],
  controllers: [PrecriptionsController],
  providers: [PrecriptionsService],
})
export class PrecriptionsModule {}
