import { Test, TestingModule } from '@nestjs/testing';
import { PrecriptionsController } from './precriptions.controller';
import { PrecriptionsService } from './precriptions.service';

describe('PrecriptionsController', () => {
  let controller: PrecriptionsController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [PrecriptionsController],
      providers: [PrecriptionsService],
    }).compile();

    controller = module.get<PrecriptionsController>(PrecriptionsController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
