import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
} from '@nestjs/common';
import { PrecriptionsService } from './precriptions.service';
import { CreatePrecriptionDto } from './dto/create-precription.dto';
import { UpdatePrecriptionDto } from './dto/update-precription.dto';

@Controller('precriptions')
export class PrecriptionsController {
  constructor(private readonly precriptionsService: PrecriptionsService) {}

  @Post()
  create(@Body() createPrecriptionDto: CreatePrecriptionDto) {
    return this.precriptionsService.create(createPrecriptionDto);
  }

  @Get()
  findAll() {
    return this.precriptionsService.findAll();
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.precriptionsService.findOne(+id);
  }

  @Patch(':id')
  update(
    @Param('id') id: string,
    @Body() updatePrecriptionDto: UpdatePrecriptionDto,
  ) {
    return this.precriptionsService.update(+id, updatePrecriptionDto);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.precriptionsService.remove(+id);
  }
}
