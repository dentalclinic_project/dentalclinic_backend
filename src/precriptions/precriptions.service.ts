import { Injectable, NotFoundException } from '@nestjs/common';
import { CreatePrecriptionDto } from './dto/create-precription.dto';
import { UpdatePrecriptionDto } from './dto/update-precription.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { PrecribingMedicine } from 'src/precribing-medicines/entities/precribing-medicine.entity';
import { Repository } from 'typeorm';
import { Precription } from './entities/precription.entity';

@Injectable()
export class PrecriptionsService {
  constructor(
    @InjectRepository(Precription)
    private precriptionsRepository: Repository<Precription>,
    @InjectRepository(PrecribingMedicine)
    private preMedicinesRepository: Repository<PrecribingMedicine>,
  ) {}

  async create(createPrecriptionDto: CreatePrecriptionDto) {
    const preMedicine = await this.preMedicinesRepository.findOneBy({
      pre_medicine_id: createPrecriptionDto.precriptionId,
    });

    const pricription: Precription = new Precription();
    pricription.precription_date = createPrecriptionDto.precription_date;
    pricription.precription_preMedicine =
      createPrecriptionDto.precription_preMedicine;
    pricription.preMedicine = preMedicine;

    await this.precriptionsRepository.save(pricription);
    return await this.precriptionsRepository.findOne({
      where: { precription_id: pricription.precription_id },
      relations: ['preMedicine'],
    });
  }

  findAll() {
    return this.precriptionsRepository.find({
      relations: ['preMedicine'],
    });
  }

  findOne(id: number) {
    return this.precriptionsRepository.findOneBy({ precription_id: id });
  }

  async update(
    precription_id: number,
    updatePrecriptionDto: UpdatePrecriptionDto,
  ) {
    const pricription = await this.precriptionsRepository.findOneBy({
      precription_id,
    });
    if (!pricription) {
      throw new NotFoundException();
    }
    const updatedPricription = {
      ...pricription,
      ...updatePrecriptionDto,
    };
    return this.precriptionsRepository.save(updatedPricription);
  }

  async remove(precription_id: number) {
    const pricription = await this.precriptionsRepository.findOneBy({
      precription_id,
    });
    return this.precriptionsRepository.remove(pricription);
  }
}
