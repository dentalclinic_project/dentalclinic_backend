import { Test, TestingModule } from '@nestjs/testing';
import { PrecriptionsService } from './precriptions.service';

describe('PrecriptionsService', () => {
  let service: PrecriptionsService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [PrecriptionsService],
    }).compile();

    service = module.get<PrecriptionsService>(PrecriptionsService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
