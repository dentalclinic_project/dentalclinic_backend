import { PrecribingMedicine } from 'src/precribing-medicines/entities/precribing-medicine.entity';
import {
  Column,
  Entity,
  PrimaryGeneratedColumn,
  ManyToOne,
  JoinColumn,
} from 'typeorm';

@Entity()
export class Precription {
  @PrimaryGeneratedColumn()
  precription_id: number;

  @Column()
  precription_date: string;

  @Column()
  precription_preMedicine: string;

  @ManyToOne(() => PrecribingMedicine, (preMedicine) => preMedicine.precription)
  @JoinColumn({ name: 'precriptionId' })
  preMedicine: PrecribingMedicine;
}
