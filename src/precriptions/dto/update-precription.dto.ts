import { PartialType } from '@nestjs/mapped-types';
import { CreatePrecriptionDto } from './create-precription.dto';

export class UpdatePrecriptionDto extends PartialType(CreatePrecriptionDto) {}
