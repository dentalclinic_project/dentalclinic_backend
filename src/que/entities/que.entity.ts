import { Doctor } from 'src/doctors/entities/doctor.entity';
import { Patien } from 'src/patiens/entities/patien.entity';
import { TreatmentType } from 'src/treatment-type/entities/treatment-type.entity';
import {
  Column,
  Entity,
  JoinColumn,
  ManyToOne,
  OneToOne,
  PrimaryGeneratedColumn,
} from 'typeorm';

@Entity()
export class Que {
  @PrimaryGeneratedColumn()
  que_id: number;

  @Column()
  que_name: string;

  @Column()
  que_surname: string;

  @Column()
  que_cardnumber: string;

  @Column()
  que_phonenumber: string;

  @Column()
  que_date: string;

  @Column()
  que_time: string;

  @Column()
  que_treatmenttype: string;

  @Column()
  que_doctor: string;

  @ManyToOne(() => Patien, (patien) => patien.que)
  @JoinColumn({ name: 'patienId' })
  patien: Patien;

  @ManyToOne(() => TreatmentType, (treatmenttype) => treatmenttype.que)
  @JoinColumn({ name: 'treatmenttypeId' })
  treatmenttype: TreatmentType;

  @ManyToOne(() => Doctor, (doctor) => doctor.que)
  @JoinColumn({ name: 'doctorId' }) // ระบุคอลัมน์ที่ใช้ในการเชื่อมโยงระหว่าง Appointment และ Doctor
  doctor: Doctor;
}
