import { Module } from '@nestjs/common';
import { QueService } from './que.service';
import { QueController } from './que.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Que } from './entities/que.entity';
import { Doctor } from 'src/doctors/entities/doctor.entity';
import { Patien } from 'src/patiens/entities/patien.entity';
import { TreatmentType } from 'src/treatment-type/entities/treatment-type.entity';

@Module({
  imports: [TypeOrmModule.forFeature([Que, Doctor, Patien, TreatmentType])],
  controllers: [QueController],
  providers: [QueService],
})
export class QueModule {}
