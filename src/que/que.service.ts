import { Injectable, NotFoundException } from '@nestjs/common';
import { CreateQueDto } from './dto/create-que.dto';
import { UpdateQueDto } from './dto/update-que.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { Que } from './entities/que.entity';
import { Repository } from 'typeorm';
import { Doctor } from 'src/doctors/entities/doctor.entity';
import { Patien } from 'src/patiens/entities/patien.entity';
import { TreatmentType } from 'src/treatment-type/entities/treatment-type.entity';

@Injectable()
export class QueService {
  constructor(
    @InjectRepository(Que)
    private quesRepository: Repository<Que>,
    @InjectRepository(Doctor)
    private doctorsRepository: Repository<Doctor>,
    @InjectRepository(Patien)
    private patiensRepository: Repository<Patien>,
    @InjectRepository(TreatmentType)
    private treatmenttypeRepository: Repository<TreatmentType>,
  ) {}

  async create(createQueDto: CreateQueDto) {
    const doctor = await this.doctorsRepository.findOneBy({
      doctor_id: createQueDto.doctorId,
    });
    const patien = await this.patiensRepository.findOneBy({
      patien_id: createQueDto.patienId,
    });
    const treatmenttype = await this.treatmenttypeRepository.findOneBy({
      treatmenttype_id: createQueDto.treatmenttypeId,
    });

    const que: Que = new Que();
    que.patien = patien;
    que.treatmenttype = treatmenttype;
    que.doctor = doctor;
    que.que_name = createQueDto.que_name;
    que.que_surname = createQueDto.que_surname;
    que.que_cardnumber = createQueDto.que_cardnumber;
    que.que_phonenumber = createQueDto.que_phonenumber;
    que.que_date = createQueDto.que_date;
    que.que_time = createQueDto.que_time;
    que.que_treatmenttype = createQueDto.que_treatmenttype;
    que.que_doctor = createQueDto.que_doctor;

    await this.quesRepository.save(que);
    return await this.quesRepository.findOne({
      where: { que_id: que.que_id },
      relations: ['patien', 'treatmenttype', 'doctor'],
    });
  }

  findAll() {
    return this.quesRepository.find({
      relations: ['patien', 'treatmenttype', 'doctor'],
    });
  }

  findOne(que_id: number) {
    return this.quesRepository.findOneBy({ que_id });
  }

  async update(que_id: number, updateQueDto: UpdateQueDto) {
    const que = await this.quesRepository.findOneBy({ que_id });
    if (!que) {
      throw new NotFoundException();
    }
    const updatedQue = { ...que, ...updateQueDto };
    return this.quesRepository.save(updatedQue);
  }

  async remove(que_id: number) {
    const que = await this.quesRepository.findOneBy({ que_id });
    return this.quesRepository.remove(que);
  }
}
