import { Test, TestingModule } from '@nestjs/testing';
import { QueController } from './que.controller';
import { QueService } from './que.service';

describe('QueController', () => {
  let controller: QueController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [QueController],
      providers: [QueService],
    }).compile();

    controller = module.get<QueController>(QueController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
