import { Test, TestingModule } from '@nestjs/testing';
import { QueService } from './que.service';

describe('QueService', () => {
  let service: QueService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [QueService],
    }).compile();

    service = module.get<QueService>(QueService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
