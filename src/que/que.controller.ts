import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
} from '@nestjs/common';
import { QueService } from './que.service';
import { CreateQueDto } from './dto/create-que.dto';
import { UpdateQueDto } from './dto/update-que.dto';

@Controller('que')
export class QueController {
  constructor(private readonly queService: QueService) {}

  @Post()
  create(@Body() createQueDto: CreateQueDto) {
    return this.queService.create(createQueDto);
  }

  @Get()
  findAll() {
    return this.queService.findAll();
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.queService.findOne(+id);
  }

  @Patch(':id')
  update(@Param('id') id: string, @Body() updateQueDto: UpdateQueDto) {
    return this.queService.update(+id, updateQueDto);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.queService.remove(+id);
  }
}
