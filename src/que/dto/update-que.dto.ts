import { PartialType } from '@nestjs/mapped-types';
import { CreateQueDto } from './create-que.dto';

export class UpdateQueDto extends PartialType(CreateQueDto) {}
