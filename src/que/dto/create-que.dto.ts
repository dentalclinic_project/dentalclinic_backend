export class CreateQueDto {
  que_name: string;

  que_surname: string;

  que_cardnumber: string;

  que_phonenumber: string;

  que_date: string;

  que_time: string;

  que_treatmenttype: string;

  que_doctor: string;

  patienId: number;

  treatmenttypeId: number;

  doctorId: number;
}
