import { Test, TestingModule } from '@nestjs/testing';
import { MedicineTypesService } from './medicine-types.service';

describe('MedicineTypesService', () => {
  let service: MedicineTypesService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [MedicineTypesService],
    }).compile();

    service = module.get<MedicineTypesService>(MedicineTypesService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
