import { Test, TestingModule } from '@nestjs/testing';
import { MedicineTypesController } from './medicine-types.controller';
import { MedicineTypesService } from './medicine-types.service';

describe('MedicineTypesController', () => {
  let controller: MedicineTypesController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [MedicineTypesController],
      providers: [MedicineTypesService],
    }).compile();

    controller = module.get<MedicineTypesController>(MedicineTypesController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
