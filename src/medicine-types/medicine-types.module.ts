import { Module } from '@nestjs/common';
import { MedicineTypesService } from './medicine-types.service';
import { MedicineTypesController } from './medicine-types.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { MedicineType } from './entities/medicine-type.entity';

@Module({
  imports: [TypeOrmModule.forFeature([MedicineType])],
  controllers: [MedicineTypesController],
  providers: [MedicineTypesService],
})
export class MedicineTypesModule {}
