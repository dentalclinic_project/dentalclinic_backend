import { Medicine } from 'src/medicines/entities/medicine.entity';
import { Column, Entity, OneToMany, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class MedicineType {
  @PrimaryGeneratedColumn()
  medicinetype_id: number;

  @Column()
  medicinetype_name: string;

  @Column()
  medicinetype_unit: string;

  @OneToMany(() => Medicine, (medicine) => medicine.medicineType)
  medicine: Medicine;
}
