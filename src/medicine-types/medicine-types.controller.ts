import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
} from '@nestjs/common';
import { MedicineTypesService } from './medicine-types.service';
import { CreateMedicineTypeDto } from './dto/create-medicine-type.dto';
import { UpdateMedicineTypeDto } from './dto/update-medicine-type.dto';

@Controller('medicine-types')
export class MedicineTypesController {
  constructor(private readonly medicineTypesService: MedicineTypesService) {}

  @Post()
  create(@Body() createMedicineTypeDto: CreateMedicineTypeDto) {
    return this.medicineTypesService.create(createMedicineTypeDto);
  }

  @Get()
  findAll() {
    return this.medicineTypesService.findAll();
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.medicineTypesService.findOne(+id);
  }

  @Patch(':id')
  update(
    @Param('id') id: string,
    @Body() updateMedicineTypeDto: UpdateMedicineTypeDto,
  ) {
    return this.medicineTypesService.update(+id, updateMedicineTypeDto);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.medicineTypesService.remove(+id);
  }
}
