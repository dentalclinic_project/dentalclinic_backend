import { Injectable } from '@nestjs/common';
import { CreateMedicineTypeDto } from './dto/create-medicine-type.dto';
import { UpdateMedicineTypeDto } from './dto/update-medicine-type.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { MedicineType } from './entities/medicine-type.entity';
import { Repository } from 'typeorm';

@Injectable()
export class MedicineTypesService {
  constructor(
    @InjectRepository(MedicineType)
    private medicineTypesRepository: Repository<MedicineType>,
  ) {}

  create(createMedicineTypeDto: CreateMedicineTypeDto) {
    const medicineType: MedicineType = new MedicineType();
    medicineType.medicinetype_name = createMedicineTypeDto.medicinetype_name;
    medicineType.medicinetype_unit = createMedicineTypeDto.medicinetype_unit;
    return this.medicineTypesRepository.save(medicineType);
  }

  findAll(): Promise<MedicineType[]> {
    return this.medicineTypesRepository.find();
  }

  findOne(id: number): Promise<MedicineType> {
    return this.medicineTypesRepository.findOneBy({ medicinetype_id: id });
  }

  async update(id: number, updateMedicineTypeDto: UpdateMedicineTypeDto) {
    const medicineType = await this.medicineTypesRepository.findOneBy({
      medicinetype_id: id,
    });
    const updatedMedicineType = { ...medicineType, ...updateMedicineTypeDto };
    return this.medicineTypesRepository.save(updatedMedicineType);
  }

  async remove(id: number) {
    const medicineType = await this.medicineTypesRepository.findOneBy({
      medicinetype_id: id,
    });
    return this.medicineTypesRepository.remove(medicineType);
  }
}
