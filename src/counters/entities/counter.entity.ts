import { Dispensing } from 'src/dispensings/entities/dispensing.entity';
import { Payment } from 'src/payments/entities/payment.entity';
import { PrecribingMedicine } from 'src/precribing-medicines/entities/precribing-medicine.entity';
import { Column, Entity, OneToMany, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class Counter {
  @PrimaryGeneratedColumn()
  counter_id: number;

  @Column()
  counter_name: string;

  @Column()
  counter_surname: string;

  @Column()
  counter_cardnumber: string;

  @Column()
  counter_phonenumber: string;

  @Column()
  counter_position: string;

  @Column()
  counter_address: string;

  @Column()
  counter_gender: string;

  @OneToMany(() => PrecribingMedicine, (preMedicine) => preMedicine.counter)
  preMedicine: PrecribingMedicine;

  @OneToMany(() => Dispensing, (dispensing) => dispensing.counter)
  dispensing: Dispensing;

  @OneToMany(() => Payment, (payment) => payment.counter)
  payment: Payment;
}
