export class CreateCounterDto {
  counter_id: number;

  counter_name: string;

  counter_surname: string;

  counter_cardnumber: string;

  counter_phonenumber: string;

  counter_position: string;

  counter_address: string;

  counter_gender: string;
}
