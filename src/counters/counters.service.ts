import { Injectable } from '@nestjs/common';
import { CreateCounterDto } from './dto/create-counter.dto';
import { UpdateCounterDto } from './dto/update-counter.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { Counter } from './entities/counter.entity';
import { Repository } from 'typeorm';

@Injectable()
export class CountersService {
  constructor(
    @InjectRepository(Counter)
    private countersRepository: Repository<Counter>,
  ) {}

  create(createCounterDto: CreateCounterDto): Promise<Counter> {
    const counter: Counter = new Counter();
    counter.counter_name = createCounterDto.counter_name;
    counter.counter_surname = createCounterDto.counter_surname;
    counter.counter_cardnumber = createCounterDto.counter_cardnumber;
    counter.counter_phonenumber = createCounterDto.counter_phonenumber;
    counter.counter_position = createCounterDto.counter_position;
    counter.counter_address = createCounterDto.counter_address;
    counter.counter_gender = createCounterDto.counter_gender;
    return this.countersRepository.save(counter);
  }

  findAll(): Promise<Counter[]> {
    return this.countersRepository.find();
  }

  findOne(id: number): Promise<Counter> {
    return this.countersRepository.findOneBy({ counter_id: id });
  }

  async update(id: number, updateCounterDto: UpdateCounterDto) {
    const counter = await this.countersRepository.findOneBy({ counter_id: id });
    const updateCounter = { ...counter, ...updateCounterDto };
    return this.countersRepository.save(updateCounter);
  }

  async remove(id: number) {
    const counter = await this.countersRepository.findOneBy({ counter_id: id });
    return this.countersRepository.remove(counter);
  }
}
