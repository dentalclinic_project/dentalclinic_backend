import { Dispensing } from 'src/dispensings/entities/dispensing.entity';
import { MedicineType } from 'src/medicine-types/entities/medicine-type.entity';
import { PrecribingDetail } from 'src/precribing-details/entities/precribing-detail.entity';
import {
  Column,
  Entity,
  PrimaryGeneratedColumn,
  ManyToOne,
  JoinColumn,
  OneToMany,
} from 'typeorm';

@Entity()
export class Medicine {
  @PrimaryGeneratedColumn()
  medicine_id: number;

  @Column()
  medicine_name: string;

  @Column()
  medicine_remaining: number;

  @Column()
  medicine_cost: number;

  @Column()
  medicine_sell: number;

  @Column()
  medicine_medicineType: string;

  @Column()
  medicine_status: string;

  @ManyToOne(() => MedicineType, (medicineType) => medicineType.medicine)
  @JoinColumn({ name: 'medicineTypeId' })
  medicineType: MedicineType;

  @OneToMany(
    () => PrecribingDetail,
    (precribingDetail) => precribingDetail.medicine,
  )
  precribingDetail: PrecribingDetail;

  @OneToMany(() => Dispensing, (dispensing) => dispensing.medicine)
  dispensing: Dispensing;
}
