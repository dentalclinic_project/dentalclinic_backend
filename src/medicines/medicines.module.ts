import { Module } from '@nestjs/common';
import { MedicinesService } from './medicines.service';
import { MedicinesController } from './medicines.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { MedicineType } from 'src/medicine-types/entities/medicine-type.entity';
import { Medicine } from './entities/medicine.entity';

@Module({
  imports: [TypeOrmModule.forFeature([Medicine, MedicineType])],
  controllers: [MedicinesController],
  providers: [MedicinesService],
})
export class MedicinesModule {}
