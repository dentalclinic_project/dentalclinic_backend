import { Injectable, NotFoundException } from '@nestjs/common';
import { CreateMedicineDto } from './dto/create-medicine.dto';
import { UpdateMedicineDto } from './dto/update-medicine.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { MedicineType } from 'src/medicine-types/entities/medicine-type.entity';
import { Repository } from 'typeorm';
import { Medicine } from './entities/medicine.entity';

@Injectable()
export class MedicinesService {
  constructor(
    @InjectRepository(Medicine)
    private medicinesRepository: Repository<Medicine>,
    @InjectRepository(MedicineType)
    private medicineTypesRepository: Repository<MedicineType>,
  ) {}

  async create(createMedicineDto: CreateMedicineDto) {
    const medicinetype = await this.medicineTypesRepository.findOneBy({
      medicinetype_id: createMedicineDto.medicineTypeId,
    });

    const medicine: Medicine = new Medicine();
    medicine.medicine_name = createMedicineDto.medicine_name;
    medicine.medicine_remaining = createMedicineDto.medicine_remaining;
    medicine.medicine_cost = createMedicineDto.medicine_cost;
    medicine.medicine_sell = createMedicineDto.medicine_sell;
    medicine.medicine_medicineType = createMedicineDto.medicine_medicineType;
    medicine.medicine_status = createMedicineDto.medicine_status;
    medicine.medicineType = medicinetype;

    await this.medicinesRepository.save(medicine);
    return await this.medicinesRepository.findOne({
      where: { medicine_id: medicine.medicine_id },
      relations: ['medicineType'],
    });
  }

  findAll() {
    return this.medicinesRepository.find({
      relations: ['medicineType'],
    });
  }

  findOne(id: number) {
    return this.medicinesRepository.findOneBy({ medicine_id: id });
  }

  async update(medicine_id: number, updateMedicineDto: UpdateMedicineDto) {
    const medicine = await this.medicinesRepository.findOneBy({
      medicine_id,
    });
    if (!medicine) {
      throw new NotFoundException();
    }
    const updatedMedicine = { ...medicine, ...updateMedicineDto };
    return this.medicinesRepository.save(updatedMedicine);
  }

  async remove(medicine_id: number) {
    const medicine = await this.medicinesRepository.findOneBy({
      medicine_id,
    });
    return this.medicinesRepository.remove(medicine);
  }
}
