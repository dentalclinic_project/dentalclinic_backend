export class CreatePlanDto {
  plan_date: string;

  plan_patien: string;

  plan_treatment_type: string;

  plan_detail: string;

  plan_phase: string;

  plan_doctor: string;

  plan_status: string;

  patienId: number;

  doctorId: number;

  treatmenttypeId: number;

  treatmentDetailId: number;
}
