import { Module } from '@nestjs/common';
import { PlansService } from './plans.service';
import { PlansController } from './plans.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Plan } from './entities/plan.entity';
import { Doctor } from 'src/doctors/entities/doctor.entity';
import { Patien } from 'src/patiens/entities/patien.entity';
import { TreatmentType } from 'src/treatment-type/entities/treatment-type.entity';
import { TreatmentDetail } from 'src/treatment-details/entities/treatment-detail.entity';

@Module({
  imports: [
    TypeOrmModule.forFeature([
      Plan,
      Doctor,
      Patien,
      TreatmentType,
      TreatmentDetail,
    ]),
  ],
  controllers: [PlansController],
  providers: [PlansService],
})
export class PlansModule {}
