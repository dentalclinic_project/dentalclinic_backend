import { Doctor } from 'src/doctors/entities/doctor.entity';
import { Patien } from 'src/patiens/entities/patien.entity';
import { TreatmentDetail } from 'src/treatment-details/entities/treatment-detail.entity';
import { TreatmentType } from 'src/treatment-type/entities/treatment-type.entity';
import {
  Column,
  Entity,
  JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn,
} from 'typeorm';

@Entity()
export class Plan {
  @PrimaryGeneratedColumn()
  plan_id: number;

  @Column()
  plan_date: string;

  @Column()
  plan_patien: string;

  @Column()
  plan_treatment_type: string;

  @Column()
  plan_detail: string;

  @Column()
  plan_phase: string;

  @Column()
  plan_doctor: string;

  @Column()
  plan_status: string;

  @ManyToOne(() => Doctor, (doctor) => doctor.plan)
  @JoinColumn({ name: 'doctorId' }) // ระบุคอลัมน์ที่ใช้ในการเชื่อมโยงระหว่าง Appointment และ Doctor
  doctor: Doctor;

  @ManyToOne(() => Patien, (patien) => patien.plan)
  @JoinColumn({ name: 'patienId' })
  patien: Patien;

  @ManyToOne(() => TreatmentType, (treatmenttype) => treatmenttype.plan)
  @JoinColumn({ name: 'treatmenttypeId' })
  treatmenttype: TreatmentType;

  @ManyToOne(() => TreatmentDetail, (treatmentDetail) => treatmentDetail.plan)
  @JoinColumn({ name: 'treatmentDetailId' })
  treatmentDetail: TreatmentDetail;
}
