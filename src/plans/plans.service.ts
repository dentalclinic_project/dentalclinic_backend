import { Injectable, NotFoundException } from '@nestjs/common';
import { CreatePlanDto } from './dto/create-plan.dto';
import { UpdatePlanDto } from './dto/update-plan.dto';
import { Plan } from './entities/plan.entity';
import { Repository } from 'typeorm';
import { Doctor } from 'src/doctors/entities/doctor.entity';
import { Patien } from 'src/patiens/entities/patien.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { TreatmentType } from 'src/treatment-type/entities/treatment-type.entity';
import { TreatmentDetail } from 'src/treatment-details/entities/treatment-detail.entity';

@Injectable()
export class PlansService {
  constructor(
    @InjectRepository(Plan)
    private plansRepository: Repository<Plan>,
    @InjectRepository(Doctor)
    private doctorsRepository: Repository<Doctor>,
    @InjectRepository(Patien)
    private patiensRepository: Repository<Patien>,
    @InjectRepository(TreatmentType)
    private treatmenttypeRepository: Repository<TreatmentType>,
    @InjectRepository(TreatmentDetail)
    private treatmentDetailRepository: Repository<TreatmentDetail>,
  ) {}

  async create(createPlanDto: CreatePlanDto) {
    const doctor = await this.doctorsRepository.findOneBy({
      doctor_id: createPlanDto.doctorId,
    });
    const patien = await this.patiensRepository.findOneBy({
      patien_id: createPlanDto.patienId,
    });
    const treatmenttype = await this.treatmenttypeRepository.findOneBy({
      treatmenttype_id: createPlanDto.treatmenttypeId,
    });
    const treatmentDetail = await this.treatmentDetailRepository.findOneBy({
      treatment_detail_id: createPlanDto.treatmentDetailId,
    });

    const plan: Plan = new Plan();
    plan.plan_date = createPlanDto.plan_date;
    plan.plan_patien = createPlanDto.plan_patien;
    plan.plan_treatment_type = createPlanDto.plan_treatment_type;
    plan.plan_detail = createPlanDto.plan_detail;
    plan.plan_phase = createPlanDto.plan_phase;
    plan.plan_doctor = createPlanDto.plan_doctor;
    plan.plan_status = createPlanDto.plan_status;
    plan.doctor = doctor;
    plan.patien = patien;
    plan.treatmenttype = treatmenttype;
    plan.treatmentDetail = treatmentDetail;

    await this.plansRepository.save(plan);
    return await this.plansRepository.findOne({
      where: { plan_id: plan.plan_id },
      relations: ['patien', 'doctor', 'treatmenttype', 'treatmentDetail'],
    });
  }

  findAll() {
    return this.plansRepository.find({
      relations: ['patien', 'doctor', 'treatmenttype', 'treatmentDetail'],
    });
  }

  findOne(id: number) {
    return this.plansRepository.findOneBy({ plan_id: id });
  }

  async update(plan_id: number, updatePlanDto: UpdatePlanDto) {
    const plan = await this.plansRepository.findOneBy({ plan_id });
    if (!plan) {
      throw new NotFoundException();
    }
    const updatedPlan = { ...plan, ...updatePlanDto };
    return this.plansRepository.save(updatedPlan);
  }

  async remove(plan_id: number) {
    const plan = await this.plansRepository.findOneBy({ plan_id });
    return this.plansRepository.remove(plan);
  }
}
