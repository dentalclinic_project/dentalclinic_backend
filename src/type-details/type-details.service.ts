import { Injectable } from '@nestjs/common';
import { CreateTypeDetailDto } from './dto/create-type-detail.dto';
import { UpdateTypeDetailDto } from './dto/update-type-detail.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { TypeDetail } from './entities/type-detail.entity';
import { Repository } from 'typeorm';
import { TreatmentType } from 'src/treatment-type/entities/treatment-type.entity';

@Injectable()
export class TypeDetailsService {
  constructor(
    @InjectRepository(TypeDetail)
    private typeDetailsRepository: Repository<TypeDetail>,
    @InjectRepository(TreatmentType)
    private treatmentTypesRepository: Repository<TreatmentType>,
  ) {}

  async create(createTypeDetailDto: CreateTypeDetailDto) {
    const treatmentType = await this.treatmentTypesRepository.findOneBy({
      treatmenttype_id: createTypeDetailDto.treatmentType,
    });

    const typeDetail: TypeDetail = new TypeDetail();
    typeDetail.typeDetail_service = createTypeDetailDto.typeDetail_service;
    typeDetail.typeDetail_unitService =
      createTypeDetailDto.typeDetail_unitService;
    typeDetail.typeDetail_price = createTypeDetailDto.typeDetail_price;
    typeDetail.treatmentType = treatmentType;

    await this.typeDetailsRepository.save(typeDetail);
    return await this.typeDetailsRepository.findOne({
      where: { typeDetail_id: typeDetail.typeDetail_id },
      relations: ['treatmentType'],
    });
  }

  // async findAll(query): Promise<Paginate> {
  //   const treatment = query.treatment || 1;
  //   const page = query.page || 1;
  //   const take = query.take || 10;
  //   const skip = (page - 1) * take;
  //   const keyword = query.keyword || '';
  //   const orderBy = query.orderBy || 'treatmenttype_name';
  //   const order = query.order || 'ASC';
  //   const currentPage = page;

  //   const [result, total] = await this.typeDetailsRepository.findAndCount({
  //     relations: ['treatmentType'],
  //     where: { typeDetail_service: Like(`%${keyword}%`) },
  //     order: { [orderBy]: order },
  //   });
  //   const lastPage = Math.ceil(total / take);
  //   return {
  //     data: result,
  //     count: total,
  //     currentPage: currentPage,
  //     lastPage: lastPage,
  //   };
  // }

  findAll() {
    return this.typeDetailsRepository.find({
      relations: ['treatmentType'],
    });
  }

  findOne(id: number) {
    return `This action returns a #${id} typeDetail`;
  }

  update(id: number, updateTypeDetailDto: UpdateTypeDetailDto) {
    return `This action updates a #${id} typeDetail`;
  }

  remove(id: number) {
    return `This action removes a #${id} typeDetail`;
  }
}
