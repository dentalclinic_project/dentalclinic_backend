import { Test, TestingModule } from '@nestjs/testing';
import { TypeDetailsController } from './type-details.controller';
import { TypeDetailsService } from './type-details.service';

describe('TypeDetailsController', () => {
  let controller: TypeDetailsController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [TypeDetailsController],
      providers: [TypeDetailsService],
    }).compile();

    controller = module.get<TypeDetailsController>(TypeDetailsController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
