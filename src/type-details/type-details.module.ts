import { Module } from '@nestjs/common';
import { TypeDetailsService } from './type-details.service';
import { TypeDetailsController } from './type-details.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { TypeDetail } from './entities/type-detail.entity';
import { TreatmentType } from 'src/treatment-type/entities/treatment-type.entity';

@Module({
  imports: [TypeOrmModule.forFeature([TypeDetail, TreatmentType])],
  controllers: [TypeDetailsController],
  providers: [TypeDetailsService],
})
export class TypeDetailsModule {}
