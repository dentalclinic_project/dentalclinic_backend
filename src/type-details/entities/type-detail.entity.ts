import { TreatmentDetail } from 'src/treatment-details/entities/treatment-detail.entity';
import { TreatmentType } from 'src/treatment-type/entities/treatment-type.entity';
import {
  Column,
  Entity,
  JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn,
} from 'typeorm';

@Entity()
export class TypeDetail {
  @PrimaryGeneratedColumn()
  typeDetail_id: number;

  @Column()
  typeDetail_service: string;

  @Column()
  typeDetail_unitService: string;

  @Column()
  typeDetail_price: number;

  @ManyToOne(() => TreatmentType, (treatmentType) => treatmentType.typeDetail)
  @JoinColumn({ name: 'treatmentType' })
  treatmentType: TreatmentType;

  @ManyToOne(
    () => TreatmentDetail,
    (treatmentDetail) => treatmentDetail.typeDetail,
  )
  @JoinColumn({ name: 'treatmentDetailId' })
  treatmentDetail: TreatmentDetail;
}
