import { Test, TestingModule } from '@nestjs/testing';
import { TypeDetailsService } from './type-details.service';

describe('TypeDetailsService', () => {
  let service: TypeDetailsService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [TypeDetailsService],
    }).compile();

    service = module.get<TypeDetailsService>(TypeDetailsService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
