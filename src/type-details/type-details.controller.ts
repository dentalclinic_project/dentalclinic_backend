import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
} from '@nestjs/common';
import { TypeDetailsService } from './type-details.service';
import { CreateTypeDetailDto } from './dto/create-type-detail.dto';
import { UpdateTypeDetailDto } from './dto/update-type-detail.dto';

@Controller('type-details')
export class TypeDetailsController {
  constructor(private readonly typeDetailsService: TypeDetailsService) {}

  @Post()
  create(@Body() createTypeDetailDto: CreateTypeDetailDto) {
    return this.typeDetailsService.create(createTypeDetailDto);
  }

  @Get()
  findAll() {
    return this.typeDetailsService.findAll();
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.typeDetailsService.findOne(+id);
  }

  @Patch(':id')
  update(
    @Param('id') id: string,
    @Body() updateTypeDetailDto: UpdateTypeDetailDto,
  ) {
    return this.typeDetailsService.update(+id, updateTypeDetailDto);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.typeDetailsService.remove(+id);
  }
}
