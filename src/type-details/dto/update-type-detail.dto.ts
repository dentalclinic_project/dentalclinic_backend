import { PartialType } from '@nestjs/mapped-types';
import { CreateTypeDetailDto } from './create-type-detail.dto';

export class UpdateTypeDetailDto extends PartialType(CreateTypeDetailDto) {}
