export class CreateTypeDetailDto {
  typeDetail_service: string;

  typeDetail_unitService: string;

  typeDetail_price: number;

  treatmentType: number;
}
