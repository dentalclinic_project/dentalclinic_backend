import { Injectable, NotFoundException } from '@nestjs/common';
import { CreateTreatmentDto } from './dto/create-treatment.dto';
import { UpdateTreatmentDto } from './dto/update-treatment.dto';
import { Treatment } from './entities/treatment.entity';
import { Doctor } from 'src/doctors/entities/doctor.entity';
import { Patien } from 'src/patiens/entities/patien.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

@Injectable()
export class TreatmentsService {
  constructor(
    @InjectRepository(Treatment)
    private treatmentsRepository: Repository<Treatment>,
    @InjectRepository(Doctor)
    private doctorsRepository: Repository<Doctor>,
    @InjectRepository(Patien)
    private patiensRepository: Repository<Patien>,
  ) {}

  async create(createTreatmentDto: CreateTreatmentDto) {
    const patien = await this.patiensRepository.findOneBy({
      patien_id: createTreatmentDto.patienId,
    });
    const doctor = await this.doctorsRepository.findOneBy({
      doctor_id: createTreatmentDto.doctorId,
    });

    const treatment: Treatment = new Treatment();
    treatment.treatment_date = createTreatmentDto.treatment_date;
    treatment.treatment_patien = createTreatmentDto.treatment_patien;
    treatment.treatment_doctor = createTreatmentDto.treatment_doctor;
    treatment.treatment_price = createTreatmentDto.treatment_price;
    treatment.doctor = doctor;
    treatment.patien = patien;

    await this.treatmentsRepository.save(treatment);
    return await this.treatmentsRepository.findOne({
      where: { treatment_id: treatment.treatment_id },
      relations: ['patien', 'doctor'],
    });
  }

  findAll() {
    return this.treatmentsRepository.find({
      relations: ['patien', 'doctor'],
    });
  }

  findOne(treatment_id: number) {
    return this.treatmentsRepository.findOneBy({ treatment_id });
  }

  async update(treatment_id: number, updateTreatmentDto: UpdateTreatmentDto) {
    const treatment = await this.treatmentsRepository.findOneBy({
      treatment_id,
    });
    if (!treatment) {
      throw new NotFoundException();
    }
    const updatedTreatment = { ...treatment, ...updateTreatmentDto };
    return this.treatmentsRepository.save(updatedTreatment);
  }

  async remove(treatment_id: number) {
    const treatment = await this.treatmentsRepository.findOneBy({
      treatment_id,
    });
    return this.treatmentsRepository.remove(treatment);
  }
}
