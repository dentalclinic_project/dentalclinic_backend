export class CreateTreatmentDto {
  treatment_date: string;

  treatment_doctor: string;

  treatment_patien: string;

  treatment_price: number;

  patienId: number;

  doctorId: number;
}
