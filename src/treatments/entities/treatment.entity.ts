import { Doctor } from 'src/doctors/entities/doctor.entity';
import { Patien } from 'src/patiens/entities/patien.entity';
import { Payment } from 'src/payments/entities/payment.entity';
import {
  Column,
  Entity,
  JoinColumn,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
} from 'typeorm';

@Entity()
export class Treatment {
  @PrimaryGeneratedColumn()
  treatment_id: number;

  @Column()
  treatment_date: string;

  @Column()
  treatment_doctor: string;

  @Column()
  treatment_patien: string;

  @Column()
  treatment_price: number;

  @ManyToOne(() => Doctor, (doctor) => doctor.treatment)
  @JoinColumn({ name: 'doctorId' }) // ระบุคอลัมน์ที่ใช้ในการเชื่อมโยงระหว่าง Appointment และ Doctor
  doctor: Doctor;

  @ManyToOne(() => Patien, (patien) => patien.treatment)
  @JoinColumn({ name: 'patienId' })
  patien: Patien;

  @OneToMany(() => Payment, (payment) => payment.treatment)
  payment: Payment;
}
