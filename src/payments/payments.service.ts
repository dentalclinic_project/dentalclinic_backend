import { Injectable, NotFoundException } from '@nestjs/common';
import { CreatePaymentDto } from './dto/create-payment.dto';
import { UpdatePaymentDto } from './dto/update-payment.dto';
import { Payment } from './entities/payment.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Dispensing } from 'src/dispensings/entities/dispensing.entity';
import { Patien } from 'src/patiens/entities/patien.entity';
import { Treatment } from 'src/treatments/entities/treatment.entity';
import { Counter } from 'src/counters/entities/counter.entity';

@Injectable()
export class PaymentsService {
  constructor(
    @InjectRepository(Payment)
    private paymentsRepository: Repository<Payment>,
    @InjectRepository(Dispensing)
    private dispensingsRepository: Repository<Dispensing>,
    @InjectRepository(Patien)
    private patiensRepository: Repository<Patien>,
    @InjectRepository(Treatment)
    private treatmentsRepository: Repository<Treatment>,
    @InjectRepository(Counter)
    private countersRepository: Repository<Counter>,
  ) {}

  async create(createPaymentDto: CreatePaymentDto) {
    const dispensing = await this.dispensingsRepository.findOneBy({
      dispensing_id: createPaymentDto.dispensingId,
    });
    const patien = await this.patiensRepository.findOneBy({
      patien_id: createPaymentDto.patienId,
    });
    const treatment = await this.treatmentsRepository.findOneBy({
      treatment_id: createPaymentDto.treatmentId,
    });
    const counter = await this.countersRepository.findOneBy({
      counter_id: createPaymentDto.counterId,
    });

    const payment: Payment = new Payment();
    payment.payment_date = createPaymentDto.payment_date;
    payment.payment_amount = createPaymentDto.payment_amount;
    payment.payment_counter = createPaymentDto.payment_counter;
    payment.payment_patien = createPaymentDto.payment_patien;
    payment.payment_dispensing = createPaymentDto.payment_dispensing;
    payment.payment_treatment = createPaymentDto.payment_treatment;
    payment.counter = counter;
    payment.patien = patien;
    payment.dispensing = dispensing;
    payment.treatment = treatment;

    await this.paymentsRepository.save(payment);
    return await this.paymentsRepository.findOne({
      where: { payment_id: payment.payment_id },
      relations: ['patien', 'dispensing', 'treatment', 'counter'],
    });
  }

  findAll() {
    return this.paymentsRepository.find({
      relations: ['patien', 'dispensing', 'treatment', 'counter'],
    });
  }

  findOne(id: number) {
    return this.paymentsRepository.findOneBy({ payment_id: id });
  }

  async update(payment_id: number, updatePaymentDto: UpdatePaymentDto) {
    const payment = await this.paymentsRepository.findOneBy({
      payment_id,
    });
    if (!payment) {
      throw new NotFoundException();
    }
    const updatedPayment = { ...payment, ...updatePaymentDto };
    return this.paymentsRepository.save(updatedPayment);
  }

  async remove(payment_id: number) {
    const payment = await this.paymentsRepository.findOneBy({
      payment_id,
    });
    return this.paymentsRepository.remove(payment);
  }
}
