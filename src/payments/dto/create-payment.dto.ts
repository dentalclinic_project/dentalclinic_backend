export class CreatePaymentDto {
  payment_date: string;

  payment_amount: number;

  payment_counter: string;

  payment_patien: string;

  payment_dispensing: string;

  payment_treatment: string;

  patienId: number;

  dispensingId: number;

  counterId: number;

  treatmentId: number;
}
