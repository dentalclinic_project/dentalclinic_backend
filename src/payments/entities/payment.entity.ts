import { Counter } from 'src/counters/entities/counter.entity';
import { Dispensing } from 'src/dispensings/entities/dispensing.entity';
import { Patien } from 'src/patiens/entities/patien.entity';
import { Reciept } from 'src/reciepts/entities/reciept.entity';
import { Treatment } from 'src/treatments/entities/treatment.entity';
import {
  Column,
  Entity,
  JoinColumn,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
} from 'typeorm';

@Entity()
export class Payment {
  @PrimaryGeneratedColumn()
  payment_id: number;

  @Column()
  payment_date: string;

  @Column()
  payment_amount: number;

  @Column()
  payment_counter: string;

  @Column()
  payment_patien: string;

  @Column()
  payment_dispensing: string;

  @Column()
  payment_treatment: string;

  @ManyToOne(() => Patien, (patien) => patien.payment)
  @JoinColumn({ name: 'patienId' })
  patien: Patien;

  @ManyToOne(() => Dispensing, (dispensing) => dispensing.payment)
  @JoinColumn({ name: 'dispensingId' })
  dispensing: Dispensing;

  @ManyToOne(() => Counter, (counter) => counter.payment)
  @JoinColumn({ name: 'counterId' })
  counter: Counter;

  @ManyToOne(() => Treatment, (treatment) => treatment.payment)
  @JoinColumn({ name: 'treatmentId' })
  treatment: Treatment;

  @OneToMany(() => Reciept, (reciept) => reciept.payment)
  reciept: Reciept;
}
