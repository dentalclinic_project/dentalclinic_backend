import { Module } from '@nestjs/common';
import { PaymentsService } from './payments.service';
import { PaymentsController } from './payments.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Payment } from './entities/payment.entity';
import { Patien } from 'src/patiens/entities/patien.entity';
import { Treatment } from 'src/treatments/entities/treatment.entity';
import { Dispensing } from 'src/dispensings/entities/dispensing.entity';
import { Counter } from 'src/counters/entities/counter.entity';

@Module({
  imports: [
    TypeOrmModule.forFeature([Payment, Patien, Dispensing, Treatment, Counter]),
  ],
  controllers: [PaymentsController],
  providers: [PaymentsService],
})
export class PaymentsModule {}
