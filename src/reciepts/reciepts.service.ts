import { Injectable, NotFoundException } from '@nestjs/common';
import { CreateRecieptDto } from './dto/create-reciept.dto';
import { UpdateRecieptDto } from './dto/update-reciept.dto';
import { Reciept } from './entities/reciept.entity';
import { Repository } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';
import { Payment } from 'src/payments/entities/payment.entity';

@Injectable()
export class RecieptsService {
  constructor(
    @InjectRepository(Reciept)
    private recieptsRepository: Repository<Reciept>,
    @InjectRepository(Payment)
    private paymentsRepository: Repository<Payment>,
  ) {}

  async create(createRecieptDto: CreateRecieptDto) {
    const payment = await this.paymentsRepository.findOneBy({
      payment_id: createRecieptDto.paymentId,
    });

    const reciept: Reciept = new Reciept();
    reciept.reciept_date = createRecieptDto.reciept_date;
    reciept.reciept_payment = createRecieptDto.reciept_payment;
    reciept.payment = payment;

    await this.recieptsRepository.save(reciept);
    return await this.recieptsRepository.findOne({
      where: { reciept_id: reciept.reciept_id },
      relations: ['payment'],
    });
  }

  findAll() {
    return this.recieptsRepository.find({
      relations: ['payment'],
    });
  }

  findOne(id: number) {
    return this.recieptsRepository.findOneBy({ reciept_id: id });
  }

  async update(reciept_id: number, updateRecieptDto: UpdateRecieptDto) {
    const reciept = await this.recieptsRepository.findOneBy({
      reciept_id,
    });
    if (!reciept) {
      throw new NotFoundException();
    }
    const updatedReciept = { ...reciept, ...updateRecieptDto };
    return this.recieptsRepository.save(updatedReciept);
  }

  async remove(reciept_id: number) {
    const reciept = await this.recieptsRepository.findOneBy({
      reciept_id,
    });
    return this.recieptsRepository.remove(reciept);
  }
}
