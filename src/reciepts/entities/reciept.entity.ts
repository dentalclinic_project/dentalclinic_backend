import { Payment } from 'src/payments/entities/payment.entity';
import {
  Column,
  Entity,
  JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn,
} from 'typeorm';

@Entity()
export class Reciept {
  @PrimaryGeneratedColumn()
  reciept_id: number;

  @Column()
  reciept_date: string;

  @Column()
  reciept_payment: string;

  @ManyToOne(() => Payment, (payment) => payment.reciept)
  @JoinColumn({ name: 'paymentId' })
  payment: Payment;
}
