import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
} from '@nestjs/common';
import { TreatmentTypeService } from './treatment-type.service';
import { CreateTreatmentTypeDto } from './dto/create-treatment-type.dto';
import { UpdateTreatmentTypeDto } from './dto/update-treatment-type.dto';

@Controller('treatment-type')
export class TreatmentTypeController {
  constructor(private readonly treatmentTypeService: TreatmentTypeService) {}

  @Post()
  create(@Body() createTreatmentTypeDto: CreateTreatmentTypeDto) {
    return this.treatmentTypeService.create(createTreatmentTypeDto);
  }

  @Get()
  findAll() {
    return this.treatmentTypeService.findAll();
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.treatmentTypeService.findOne(+id);
  }

  @Patch(':id')
  update(
    @Param('id') id: string,
    @Body() updateTreatmentTypeDto: UpdateTreatmentTypeDto,
  ) {
    return this.treatmentTypeService.update(+id, updateTreatmentTypeDto);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.treatmentTypeService.remove(+id);
  }
}
