import { Module } from '@nestjs/common';
import { TreatmentTypeService } from './treatment-type.service';
import { TreatmentTypeController } from './treatment-type.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { TreatmentType } from './entities/treatment-type.entity';

@Module({
  imports: [TypeOrmModule.forFeature([TreatmentType])],
  controllers: [TreatmentTypeController],
  providers: [TreatmentTypeService],
})
export class TreatmentTypeModule {}
