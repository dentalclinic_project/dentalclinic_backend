import { Appointment } from 'src/appointments/entities/appointment.entity';
import { HisTreatment } from 'src/his_treatments/entities/his_treatment.entity';
import { Plan } from 'src/plans/entities/plan.entity';
import { Que } from 'src/que/entities/que.entity';
import { TreatmentDetail } from 'src/treatment-details/entities/treatment-detail.entity';
import { TypeDetail } from 'src/type-details/entities/type-detail.entity';
import { Column, Entity, OneToMany, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class TreatmentType {
  @PrimaryGeneratedColumn()
  treatmenttype_id: number;

  @Column()
  treatmenttype_name: string;

  @OneToMany(() => Appointment, (appointment) => appointment.treatmenttype)
  appointment: Appointment;

  @OneToMany(() => Que, (que) => que.treatmenttype)
  que: Que;

  @OneToMany(() => Plan, (plan) => plan.treatmenttype)
  plan: Plan;

  @OneToMany(
    () => TreatmentDetail,
    (treatmentDetail) => treatmentDetail.treatmenttype,
  )
  treatmentDetail: TreatmentDetail;

  @OneToMany(() => HisTreatment, (hisTreatment) => hisTreatment.treatmenttype)
  hisTreatment: HisTreatment;

  @OneToMany(() => TypeDetail, (typeDetail) => typeDetail.treatmentType)
  typeDetail: TypeDetail;
}
