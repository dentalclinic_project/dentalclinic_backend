import { Injectable } from '@nestjs/common';
import { CreateTreatmentTypeDto } from './dto/create-treatment-type.dto';
import { UpdateTreatmentTypeDto } from './dto/update-treatment-type.dto';
import { Repository } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';
import { TreatmentType } from './entities/treatment-type.entity';

@Injectable()
export class TreatmentTypeService {
  constructor(
    @InjectRepository(TreatmentType)
    private treatmentTypeRepository: Repository<TreatmentType>,
  ) {}

  create(createTreatmentTypeDto: CreateTreatmentTypeDto) {
    const treatmenttype: TreatmentType = new TreatmentType();
    treatmenttype.treatmenttype_name =
      createTreatmentTypeDto.treatmenttype_name;
    return this.treatmentTypeRepository.save(treatmenttype);
  }

  findAll(): Promise<TreatmentType[]> {
    return this.treatmentTypeRepository.find();
  }

  findOne(id: number): Promise<TreatmentType> {
    return this.treatmentTypeRepository.findOneBy({ treatmenttype_id: id });
  }

  async update(id: number, updateTreatmentTypeDto: UpdateTreatmentTypeDto) {
    const treatmenttype = await this.treatmentTypeRepository.findOneBy({
      treatmenttype_id: id,
    });
    const updateTreatmentType = { ...treatmenttype, ...updateTreatmentTypeDto };
    return this.treatmentTypeRepository.save(updateTreatmentType);
  }

  async remove(id: number) {
    const treatmenttype = await this.treatmentTypeRepository.findOneBy({
      treatmenttype_id: id,
    });
    return this.treatmentTypeRepository.remove(treatmenttype);
  }
}
